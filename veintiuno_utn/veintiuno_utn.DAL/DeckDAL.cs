﻿using Npgsql;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using veintiuno_utn.Entities;

namespace veintiuno_utn.DAL
{
    public class DeckDAL
    {
        /// <summary>
        /// Inserta una mesa
        /// </summary>
        /// <param name="mesa">objeto mesa</param>
        /// <returns>int</returns>
        public int InsertarMesa(EMesa mesa)
        {
            try
            {
                string sql = "INSERT INTO mesas (id_invitacion, pass, cantidad_max_jug, jugadores_en_mesa, partida_en_curso) " +
                    "VALUES (@invi, @pass, @max, @jug, @curso) returning id; ";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@invi", mesa.Codigo_invitacion);
                    cmd.Parameters.AddWithValue("@pass", mesa.Pass);
                    cmd.Parameters.AddWithValue("@max", mesa.Cantidad_max_jug);
                    cmd.Parameters.AddWithValue("@jug", mesa.Jugadores_en_mesa);
                    cmd.Parameters.AddWithValue("@curso", mesa.Partida_en_curso);
                    int id = (int)cmd.ExecuteScalar();
                    return id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Inserta una carta
        /// </summary>
        /// <param name="foto">image</param>
        /// <returns>int</returns>
        public int InsertarBocaAbajo(Image foto)
        {
            try
            {
                string sql = "INSERT INTO cartas (carta) VALUES (@carta) returning id; ";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    MemoryStream ms = new MemoryStream();
                    foto.Save(ms, ImageFormat.Jpeg);
                    cmd.Parameters.AddWithValue("@deck", ms.ToArray());
                    int id = (int)cmd.ExecuteScalar();
                    return id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool VerificarMesa(string id)
        {
            string sql = "SELECT deck_id FROM mesas WHERE deck_id = @deck";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@deck", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Actualiza cantidad de cartas
        /// </summary>
        /// <param name="cartas">Cantida cartas</param>
        /// <param name="codigo">codigo</param>
        public void CantidadCartas(int cartas, string codigo)
        {
            try
            {
                string sql = "UPDATE decks SET cartas_restantes = @rem WHERE codigo = @cod";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@rem", cartas);
                    cmd.Parameters.AddWithValue("@cod", codigo);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problema en update: " + ex);
            }
        }
        /// <summary>
        /// Obtiene una mesa mediante el codigo
        /// </summary>
        /// <param name="codigo">Codigo de la mesa</param>
        /// <returns>int</returns>
        public int IdMesa(string codigo)
        {
            int id = 0;
            string sql = "SELECT id FROM mesas WHERE id_invitacion = @cod";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@cod", codigo);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    int temp = CargarDatos(reader);
                    id = temp;
                    return id;
                }
            }
            return id;
        }
        private int CargarDatos(NpgsqlDataReader reader)
        {
            int id = reader.GetInt32(0);
            return id;
        }
        /// <summary>
        /// Guarda un deck en la base de datos
        /// </summary>
        /// <param name="deck">deck</param>
        /// <returns>int</returns>
        public int InsertarDecks(EDecks deck)
        {
            try
            {
                string sql = "INSERT INTO decks (codigo, id_mesa, cartas_restantes) VALUES (@cod, @mesa, @cart) returning id; ";
                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@cod", deck.Codigo);
                    cmd.Parameters.AddWithValue("@mesa", deck.Id_mesa);
                    cmd.Parameters.AddWithValue("@cart", deck.Cartas_restantes);
                    int id = (int)cmd.ExecuteScalar();
                    return id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// obtiene el deck a usar
        /// </summary>
        /// <param name="deck">objeto deck</param>
        /// <returns>booleano</returns>
        public bool DeckAUsar(EDecks deck)
        {
            string sql = "SELECT id, codigo, cartas_restantes FROM decks WHERE cartas_restantes > 0 and id_mesa = @id ORDER BY id ASC LIMIT 1";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", deck.Id_mesa);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    EDecks temp = CargarDeck(reader);
                    deck.Id = temp.Id;
                    deck.Codigo = temp.Codigo;
                    deck.Cartas_restantes = temp.Cartas_restantes;
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Carga la mesa actua
        /// </summary>
        /// <param name="mesa">Mesa</param>
        /// <returns>booleano</returns>
        public bool CargarMesaActual(EMesa mesa)
        {
            string sql = "SELECT id, id_invitacion, pass, cantidad_max_jug, jugadores_en_mesa," +
                " ganador_id, partida_en_curso FROM mesas WHERE id_invitacion = @cod";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@cod", mesa.Codigo_invitacion);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    EMesa temp = CargarMesa(reader);
                    mesa.Id = temp.Id;
                    mesa.Codigo_invitacion = temp.Codigo_invitacion;
                    mesa.Pass = temp.Pass;
                    mesa.Cantidad_max_jug = temp.Cantidad_max_jug;
                    mesa.Jugadores_en_mesa = temp.Jugadores_en_mesa;
                    mesa.Ganador = temp.Ganador;
                    mesa.Partida_en_curso = temp.Partida_en_curso;
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Carga una mesa
        /// </summary>
        /// <param name="reader">reader</param>
        /// <returns>Mesa</returns>
        private EMesa CargarMesa(NpgsqlDataReader reader)
        {
            EMesa e = new EMesa()
            {
                Id = reader.GetInt32(0),
                Codigo_invitacion = reader.GetString(1),
                Pass = reader.GetString(2),
                Cantidad_max_jug = reader.GetInt32(3),
                Jugadores_en_mesa = reader.GetInt32(4),
                Ganador = reader.GetInt32(5),
                Partida_en_curso = reader.GetBoolean(6)
            };
            return e;
        }
        /// <summary>
        /// Carga los datos de un deck
        /// </summary>
        /// <param name="reader">reade</param>
        /// <returns>Deck</returns>
        private EDecks CargarDeck(NpgsqlDataReader reader)
        {
            EDecks e = new EDecks()
            {
                Id = reader.GetInt32(0),
                Codigo = reader.GetString(1),
                Cartas_restantes = reader.GetInt32(2)
            };
            return e;
        }

    }
}
