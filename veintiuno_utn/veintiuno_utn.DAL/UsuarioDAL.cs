﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using veintiuno_utn.Entities;

namespace veintiuno_utn.DAL
{
    public class UsuarioDAL
    {
        public bool Login(EUsuario u)
        {
            string sql = "SELECT correo " +
            " FROM usuarios WHERE correo = @correo";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@correo", u.Correo);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {

                    return true;

                }
                else
                {
                    InsertarUsuario(u);
                }
            }

            return false;
        }

        public bool InsertarUsuario(EUsuario u)
        {
            try
            {
                string sql = "INSERT INTO usuarios (nombre,correo,foto,dinero_disponible)" +
                    " VALUES (@nom,@cor,@fot,@din) ";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    MemoryStream ms = new MemoryStream();
                    u.Foto.Save(ms, ImageFormat.Jpeg);
                    cmd.Parameters.AddWithValue("@nom", u.Nombre);
                    cmd.Parameters.AddWithValue("@cor", u.Correo);
                    cmd.Parameters.AddWithValue("@fot", ms.ToArray());
                    cmd.Parameters.AddWithValue("@din", u.dinero_disponible);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }

        public void InsertarApuesta(double apuesta,int  id)
        {
            try
            {
                string sql = "UPDATE usuarios SET dinero_disponible = @din  WHERE id = @id";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@din", apuesta);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problema en update: " + ex);
                
            }
        }





        public int id(string correo)
        {
            int id = 0;
            string sql = "SELECT id FROM usuarios WHERE correo = @corr";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@corr", correo);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    id = reader.GetInt32(0);
                }
            }
            return id;
        }


        public double ConsultarDinero(int id)
        {
            double dinero = 0;
            string sql = "SELECT dinero_disponible FROM usuarios WHERE id = @id";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    dinero = reader.GetDouble(0);
                    return dinero;
                }
            }
            return dinero;
        }



        public EUsuario CargarUsuarios(int id)
        {
            EUsuario u = new EUsuario();
            string sql = "SELECT id, nombre, correo, foto FROM usuarios WHERE id = @id";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    EUsuario temp = CargarUsuarios(reader);
                    u.Id = temp.Id;
                    u.Nombre = temp.Nombre;
                    u.Correo = temp.Correo;
                    u.Foto = temp.Foto;
                    return u;
                }
            }
            return u;
        }
        private EUsuario CargarUsuarios(NpgsqlDataReader reader)
        {
            EUsuario temp = new EUsuario
            {
                Id = reader.GetInt32(0),
                Nombre = reader.GetString(1),
                Correo = reader.GetString(2),
                Foto = byteArrayToImage((byte[])reader["foto"])
            };
            return temp;
        }
        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            using (MemoryStream mStream = new MemoryStream(byteArrayIn))
            {
                return Image.FromStream(mStream);
            }
        }
        public void InsertarPartidaNueva(EPartida u)
        {
            try
            {
                string sql = "INSERT INTO partida (num_jugador, id_mesa, id_usuario, turno) VALUES (@num, @id_mesa, @id_us, @turn) returning Id";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@num", u.Num_jugador);
                    cmd.Parameters.AddWithValue("@id_mesa", u.Id_mesa);
                    cmd.Parameters.AddWithValue("@id_us", u.Id_usuario);
                    cmd.Parameters.AddWithValue("@turn", u.Turno);
                    u.Id = (int)cmd.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problema en update: " + ex);
            }
        }
        public void InsertarApuesta(int id, int id_mesa, double apuesta)
        {
            try
            {
                string sql = "INSERT INTO apuestas (id_usuario, id_mesa, apuesta) VALUES (@id_us, @id_mesa, @apuest)";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@id_us", id);
                    cmd.Parameters.AddWithValue("@id_mesa", id_mesa);
                    cmd.Parameters.AddWithValue("@apuest", apuesta);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problema en update: " + ex);
            }
        }

    }
}
