﻿namespace veintiuno_utn.GUI
{
    partial class FrmCartas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCartas));
            this.lblFaltantes = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPass = new System.Windows.Forms.Label();
            this.panelJugador = new System.Windows.Forms.Panel();
            this.ptbCarta9 = new System.Windows.Forms.PictureBox();
            this.ptbCarta8 = new System.Windows.Forms.PictureBox();
            this.ptbCarta13 = new System.Windows.Forms.PictureBox();
            this.ptbCarta12 = new System.Windows.Forms.PictureBox();
            this.ptbCarta11 = new System.Windows.Forms.PictureBox();
            this.ptbCarta10 = new System.Windows.Forms.PictureBox();
            this.ptbCarta7 = new System.Windows.Forms.PictureBox();
            this.ptbCarta6 = new System.Windows.Forms.PictureBox();
            this.ptbCarta5 = new System.Windows.Forms.PictureBox();
            this.ptbCarta4 = new System.Windows.Forms.PictureBox();
            this.ptbCarta3 = new System.Windows.Forms.PictureBox();
            this.btnNewCarta = new System.Windows.Forms.Button();
            this.ptbCarta2 = new System.Windows.Forms.PictureBox();
            this.ptbCarta1 = new System.Windows.Forms.PictureBox();
            this.panelMesa = new System.Windows.Forms.Panel();
            this.lblXML = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnUnirse = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.lblMess = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelJugador.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1)).BeginInit();
            this.panelMesa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFaltantes
            // 
            this.lblFaltantes.Font = new System.Drawing.Font("Century Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaltantes.Location = new System.Drawing.Point(1397, 112);
            this.lblFaltantes.Name = "lblFaltantes";
            this.lblFaltantes.Size = new System.Drawing.Size(311, 105);
            this.lblFaltantes.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1391, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Cartas restantes del deck";
            // 
            // lblPass
            // 
            this.lblPass.AutoSize = true;
            this.lblPass.Location = new System.Drawing.Point(1790, 44);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(0, 17);
            this.lblPass.TabIndex = 12;
            // 
            // panelJugador
            // 
            this.panelJugador.BackColor = System.Drawing.Color.CadetBlue;
            this.panelJugador.Controls.Add(this.ptbCarta9);
            this.panelJugador.Controls.Add(this.ptbCarta8);
            this.panelJugador.Controls.Add(this.ptbCarta13);
            this.panelJugador.Controls.Add(this.label3);
            this.panelJugador.Controls.Add(this.ptbCarta12);
            this.panelJugador.Controls.Add(this.lblFaltantes);
            this.panelJugador.Controls.Add(this.ptbCarta11);
            this.panelJugador.Controls.Add(this.ptbCarta10);
            this.panelJugador.Controls.Add(this.ptbCarta7);
            this.panelJugador.Controls.Add(this.ptbCarta6);
            this.panelJugador.Controls.Add(this.ptbCarta5);
            this.panelJugador.Controls.Add(this.ptbCarta4);
            this.panelJugador.Controls.Add(this.ptbCarta3);
            this.panelJugador.Controls.Add(this.btnNewCarta);
            this.panelJugador.Controls.Add(this.ptbCarta2);
            this.panelJugador.Controls.Add(this.ptbCarta1);
            this.panelJugador.Location = new System.Drawing.Point(1, 475);
            this.panelJugador.Name = "panelJugador";
            this.panelJugador.Size = new System.Drawing.Size(2082, 607);
            this.panelJugador.TabIndex = 14;
            // 
            // ptbCarta9
            // 
            this.ptbCarta9.AccessibleDescription = "";
            this.ptbCarta9.AccessibleName = "8";
            this.ptbCarta9.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta9.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta9.InitialImage")));
            this.ptbCarta9.Location = new System.Drawing.Point(202, 263);
            this.ptbCarta9.Name = "ptbCarta9";
            this.ptbCarta9.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta9.TabIndex = 26;
            this.ptbCarta9.TabStop = false;
            this.ptbCarta9.Visible = false;
            this.ptbCarta9.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // ptbCarta8
            // 
            this.ptbCarta8.AccessibleDescription = "";
            this.ptbCarta8.AccessibleName = "7";
            this.ptbCarta8.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta8.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta8.InitialImage")));
            this.ptbCarta8.Location = new System.Drawing.Point(18, 263);
            this.ptbCarta8.Name = "ptbCarta8";
            this.ptbCarta8.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta8.TabIndex = 25;
            this.ptbCarta8.TabStop = false;
            this.ptbCarta8.Visible = false;
            this.ptbCarta8.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // ptbCarta13
            // 
            this.ptbCarta13.AccessibleDescription = "";
            this.ptbCarta13.AccessibleName = "12";
            this.ptbCarta13.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta13.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta13.InitialImage")));
            this.ptbCarta13.Location = new System.Drawing.Point(934, 263);
            this.ptbCarta13.Name = "ptbCarta13";
            this.ptbCarta13.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta13.TabIndex = 23;
            this.ptbCarta13.TabStop = false;
            this.ptbCarta13.Visible = false;
            this.ptbCarta13.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // ptbCarta12
            // 
            this.ptbCarta12.AccessibleDescription = "";
            this.ptbCarta12.AccessibleName = "11";
            this.ptbCarta12.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta12.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta12.InitialImage")));
            this.ptbCarta12.Location = new System.Drawing.Point(748, 263);
            this.ptbCarta12.Name = "ptbCarta12";
            this.ptbCarta12.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta12.TabIndex = 22;
            this.ptbCarta12.TabStop = false;
            this.ptbCarta12.Visible = false;
            this.ptbCarta12.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // ptbCarta11
            // 
            this.ptbCarta11.AccessibleDescription = "";
            this.ptbCarta11.AccessibleName = "10";
            this.ptbCarta11.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta11.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta11.InitialImage")));
            this.ptbCarta11.Location = new System.Drawing.Point(569, 263);
            this.ptbCarta11.Name = "ptbCarta11";
            this.ptbCarta11.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta11.TabIndex = 21;
            this.ptbCarta11.TabStop = false;
            this.ptbCarta11.Visible = false;
            this.ptbCarta11.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // ptbCarta10
            // 
            this.ptbCarta10.AccessibleDescription = "";
            this.ptbCarta10.AccessibleName = "9";
            this.ptbCarta10.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta10.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta10.InitialImage")));
            this.ptbCarta10.Location = new System.Drawing.Point(386, 263);
            this.ptbCarta10.Name = "ptbCarta10";
            this.ptbCarta10.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta10.TabIndex = 20;
            this.ptbCarta10.TabStop = false;
            this.ptbCarta10.Visible = false;
            this.ptbCarta10.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // ptbCarta7
            // 
            this.ptbCarta7.AccessibleDescription = "";
            this.ptbCarta7.AccessibleName = "6";
            this.ptbCarta7.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta7.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta7.InitialImage")));
            this.ptbCarta7.Location = new System.Drawing.Point(1123, 15);
            this.ptbCarta7.Name = "ptbCarta7";
            this.ptbCarta7.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta7.TabIndex = 19;
            this.ptbCarta7.TabStop = false;
            this.ptbCarta7.Visible = false;
            this.ptbCarta7.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // ptbCarta6
            // 
            this.ptbCarta6.AccessibleDescription = "";
            this.ptbCarta6.AccessibleName = "5";
            this.ptbCarta6.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta6.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta6.InitialImage")));
            this.ptbCarta6.Location = new System.Drawing.Point(934, 15);
            this.ptbCarta6.Name = "ptbCarta6";
            this.ptbCarta6.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta6.TabIndex = 18;
            this.ptbCarta6.TabStop = false;
            this.ptbCarta6.Visible = false;
            this.ptbCarta6.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // ptbCarta5
            // 
            this.ptbCarta5.AccessibleDescription = "";
            this.ptbCarta5.AccessibleName = "4";
            this.ptbCarta5.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta5.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta5.InitialImage")));
            this.ptbCarta5.Location = new System.Drawing.Point(748, 15);
            this.ptbCarta5.Name = "ptbCarta5";
            this.ptbCarta5.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta5.TabIndex = 17;
            this.ptbCarta5.TabStop = false;
            this.ptbCarta5.Visible = false;
            this.ptbCarta5.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // ptbCarta4
            // 
            this.ptbCarta4.AccessibleDescription = "";
            this.ptbCarta4.AccessibleName = "3";
            this.ptbCarta4.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta4.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta4.InitialImage")));
            this.ptbCarta4.Location = new System.Drawing.Point(569, 15);
            this.ptbCarta4.Name = "ptbCarta4";
            this.ptbCarta4.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta4.TabIndex = 16;
            this.ptbCarta4.TabStop = false;
            this.ptbCarta4.Visible = false;
            this.ptbCarta4.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // ptbCarta3
            // 
            this.ptbCarta3.AccessibleDescription = "";
            this.ptbCarta3.AccessibleName = "2";
            this.ptbCarta3.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta3.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta3.InitialImage")));
            this.ptbCarta3.Location = new System.Drawing.Point(386, 15);
            this.ptbCarta3.Name = "ptbCarta3";
            this.ptbCarta3.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta3.TabIndex = 15;
            this.ptbCarta3.TabStop = false;
            this.ptbCarta3.Visible = false;
            this.ptbCarta3.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // btnNewCarta
            // 
            this.btnNewCarta.Location = new System.Drawing.Point(1394, 236);
            this.btnNewCarta.Name = "btnNewCarta";
            this.btnNewCarta.Size = new System.Drawing.Size(208, 34);
            this.btnNewCarta.TabIndex = 14;
            this.btnNewCarta.Text = "Nueva carta";
            this.btnNewCarta.UseVisualStyleBackColor = true;
            this.btnNewCarta.Visible = false;
            this.btnNewCarta.Click += new System.EventHandler(this.btnNewCarta_Click);
            // 
            // ptbCarta2
            // 
            this.ptbCarta2.AccessibleDescription = "";
            this.ptbCarta2.AccessibleName = "1";
            this.ptbCarta2.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta2.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta2.InitialImage")));
            this.ptbCarta2.Location = new System.Drawing.Point(202, 15);
            this.ptbCarta2.Name = "ptbCarta2";
            this.ptbCarta2.Size = new System.Drawing.Size(164, 231);
            this.ptbCarta2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta2.TabIndex = 2;
            this.ptbCarta2.TabStop = false;
            this.ptbCarta2.Visible = false;
            this.ptbCarta2.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // ptbCarta1
            // 
            this.ptbCarta1.AccessibleName = "0";
            this.ptbCarta1.InitialImage = global::veintiuno_utn.Properties.Resources.carta_boca_abajo1;
            this.ptbCarta1.Location = new System.Drawing.Point(13, 15);
            this.ptbCarta1.Name = "ptbCarta1";
            this.ptbCarta1.Size = new System.Drawing.Size(169, 231);
            this.ptbCarta1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta1.TabIndex = 0;
            this.ptbCarta1.TabStop = false;
            this.ptbCarta1.Click += new System.EventHandler(this.ptbs_Click);
            // 
            // panelMesa
            // 
            this.panelMesa.BackColor = System.Drawing.Color.CadetBlue;
            this.panelMesa.Controls.Add(this.lblXML);
            this.panelMesa.Controls.Add(this.lblTotal);
            this.panelMesa.Controls.Add(this.btnNuevo);
            this.panelMesa.Controls.Add(this.btnUnirse);
            this.panelMesa.Controls.Add(this.label2);
            this.panelMesa.Controls.Add(this.txtCodigo);
            this.panelMesa.Controls.Add(this.lblMess);
            this.panelMesa.Controls.Add(this.pictureBox1);
            this.panelMesa.Location = new System.Drawing.Point(1, 0);
            this.panelMesa.Name = "panelMesa";
            this.panelMesa.Size = new System.Drawing.Size(2052, 480);
            this.panelMesa.TabIndex = 15;
            // 
            // lblXML
            // 
            this.lblXML.AutoSize = true;
            this.lblXML.Location = new System.Drawing.Point(821, 36);
            this.lblXML.Name = "lblXML";
            this.lblXML.Size = new System.Drawing.Size(0, 17);
            this.lblXML.TabIndex = 29;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(22, 275);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(400, 110);
            this.lblTotal.TabIndex = 27;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.btnNuevo.Location = new System.Drawing.Point(16, 19);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(209, 45);
            this.btnNuevo.TabIndex = 23;
            this.btnNuevo.Text = "Comenzar nueva";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click_1);
            // 
            // btnUnirse
            // 
            this.btnUnirse.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnirse.Location = new System.Drawing.Point(16, 87);
            this.btnUnirse.Name = "btnUnirse";
            this.btnUnirse.Size = new System.Drawing.Size(209, 45);
            this.btnUnirse.TabIndex = 24;
            this.btnUnirse.Text = "Unirse";
            this.btnUnirse.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 17);
            this.label2.TabIndex = 26;
            this.label2.Text = "Ingrese el código";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigo.Location = new System.Drawing.Point(16, 169);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(189, 30);
            this.txtCodigo.TabIndex = 25;
            // 
            // lblMess
            // 
            this.lblMess.AutoSize = true;
            this.lblMess.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMess.Location = new System.Drawing.Point(13, 399);
            this.lblMess.Name = "lblMess";
            this.lblMess.Size = new System.Drawing.Size(387, 34);
            this.lblMess.TabIndex = 28;
            this.lblMess.Text = "A continuación tus cartas...";
            this.lblMess.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::veintiuno_utn.Properties.Resources.blackjack_mesa_edit2;
            this.pictureBox1.Location = new System.Drawing.Point(1181, -89);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(898, 649);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // FrmCartas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1924, 1055);
            this.Controls.Add(this.panelMesa);
            this.Controls.Add(this.panelJugador);
            this.Controls.Add(this.lblPass);
            this.Name = "FrmCartas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmCartas";
            this.Load += new System.EventHandler(this.frmCartas_Load);
            this.panelJugador.ResumeLayout(false);
            this.panelJugador.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1)).EndInit();
            this.panelMesa.ResumeLayout(false);
            this.panelMesa.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox ptbCarta1;
        private System.Windows.Forms.PictureBox ptbCarta2;
        private System.Windows.Forms.Label lblFaltantes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Panel panelJugador;
        private System.Windows.Forms.Button btnNewCarta;
        private System.Windows.Forms.PictureBox ptbCarta9;
        private System.Windows.Forms.PictureBox ptbCarta8;
        private System.Windows.Forms.PictureBox ptbCarta13;
        private System.Windows.Forms.PictureBox ptbCarta12;
        private System.Windows.Forms.PictureBox ptbCarta11;
        private System.Windows.Forms.PictureBox ptbCarta10;
        private System.Windows.Forms.PictureBox ptbCarta7;
        private System.Windows.Forms.PictureBox ptbCarta6;
        private System.Windows.Forms.PictureBox ptbCarta5;
        private System.Windows.Forms.PictureBox ptbCarta4;
        private System.Windows.Forms.PictureBox ptbCarta3;
        private System.Windows.Forms.Panel panelMesa;
        private System.Windows.Forms.Label lblXML;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnUnirse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label lblMess;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}