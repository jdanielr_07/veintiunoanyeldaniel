﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace veintiuno_utn.Entities
{
    public class ECarta
    {
        public int Id { get; set; }
        public int Id_jugador { get; set; }
        public string Carta { get; set; }
        public int Deck_id { get; set; }
        public int Id_Mesa { get; set; }
        public int Valor_num { get; set; }
    }
}
