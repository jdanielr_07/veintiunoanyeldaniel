﻿namespace veintiuno_utn.Entities
{
    public class Rootobject
    {
        public bool shuffled { get; set; }
        public string deck_id { get; set; }
        public int remaining { get; set; }
        public bool success { get; set; }
    }
}
