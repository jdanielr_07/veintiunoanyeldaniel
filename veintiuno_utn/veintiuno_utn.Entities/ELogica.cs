﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using WebService;

namespace veintiuno_utn.Entities
{
    public class ELogica
    {
        private List<XElement> li;
        private List<string> listaValores;
        private List<string> listaPng;
        private List<PictureBox> ptbs;
        private List<PictureBox> profiles_jug;
        private int nuevas_cartas;
        private XDocument xml1;
        private string codigoDeck;
        private int Restantes;
        private EUsuario logeado;
        private int jugadores;
        private int mesa_jug;
        public EMesa mesa { get; set; }
        private string tipo;
        
        private int suma(List<string> valores)
        {
            int total = 0;
            string num = "0";
            for (int q = 0; q < valores.Count; q++)
            {
                num = valores[q];
                if (valores[q].Equals("QUEEN") || valores[q].Equals("JACK") || valores[q].Equals("KING"))
                {
                    num = "10";
                }
                else if (valores[q].Equals("ACE"))
                {
                    num = "11";
                }
                total += Int32.Parse(num);
            }
            return total;
        }
              
    }
}
