﻿using System.Drawing;

namespace veintiuno_utn.Entities
{
    public class EUsuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public Image Foto { get; set; }
        public int Id_partida { get; set; }
        public double dinero_disponible { get; set; }
        public int Num_jugador { get; set; }
    }
}
