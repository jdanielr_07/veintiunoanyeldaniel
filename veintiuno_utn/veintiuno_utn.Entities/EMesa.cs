﻿using System.Collections.Generic;

namespace veintiuno_utn.Entities
{
    public class EMesa
    {
        public int Id { get; set; }
        public string Id_invitacion { get; set; }
        public string Pass { get; set; }
        public int Cantidad_max_jug { get; set; }
        public int Jugadores_en_mesa { get; set; }
        public List<int> Jugadores { get; set; }
        public string Codigo_invitacion { get; set; }
        public int Ganador { get; set; }
        public bool Partida_en_curso { get; set; }
    }
}
