﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using veintiuno_utn.DAL;
using veintiuno_utn.Entities;

namespace veintiuno_utn.BOL
{
    public class CartaBOL
    {
        /// <summary>
        /// Inserta una carta
        /// </summary>
        /// <param name="c">Objeto carta</param>
        public void InsertarCarta(ECarta c)
        {
            new CartaDAL().Insertar(c);
        }
        /// <summary>
        /// Carga las cartas a la mesa
        /// </summary>
        /// <param name="c">Objeto carta</param>
        public void CargarCartasMesa(ECarta c)
        {
            new CartaDAL().CargarCartas(c);
        }
        /// <summary>
        /// Genera una lista de cartas
        /// </summary>
        /// <param name="c">Carta</param>
        /// <returns>lista de cartas</returns>
        public List<ECarta> CargarCartasDealer(ECarta c)
        {
            return new CartaDAL().CargarCartasDealer(c);
        }
        /// <summary>
        /// Carga el valir de una carta
        /// </summary>
        /// <param name="c">objeto carta</param>
        /// <returns>total </returns>
        public int CargarValorTotal(ECarta c)
        {
            return new CartaDAL().TotalValor(c);
        }      

    }
}
