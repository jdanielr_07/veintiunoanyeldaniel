﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using veintiuno_utn.DAL;
using veintiuno_utn.Entities;

namespace veintiuno_utn.BOL
{
    public class MesaBOL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        public void EstadoPartida(EMesa m)
        {
            new MesaDAL().PartidaEstado(m);
        }
        /// <summary>
        /// Obtiene el estado de una mesa
        /// </summary>
        /// <param name="m">Mesa</param>
        /// <returns>booleano</returns>
        public bool EstadoMesa(EMesa m)
        {
            return new MesaDAL().VerificarMesa(m);
        }
        /// <summary>
        /// Actualiza los jugadores
        /// </summary>
        /// <param name="m">Mesa</param>
        public void ActualizarJugadoresMesa(EMesa m)
        {
            new MesaDAL().Jugadores(m);
        }
        /// <summary>
        /// Obtiene una lista de los usuarios que están jugando
        /// </summary>
        /// <param name="m">Mesa</param>
        /// <returns>lista de enteros</returns>
        public List<int> UsuariosPartida(EMesa m)
        {
            return new MesaDAL().Usuarios(m);
        }
    }
}
