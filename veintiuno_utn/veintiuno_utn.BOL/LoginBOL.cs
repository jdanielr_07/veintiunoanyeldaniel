﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;



namespace veintiuno_utn.BOL
{
    public class LoginBOL
    {
        public string clientID = "447984664800-85984i3apjmm0n77973cltktr4asovcn.apps.googleusercontent.com";    //Replace this with your Client ID
        public string clientSecret = "cCP1yQk5hp70sPcsoKfkkg7S";                                         //Replace this with your Client Secret
        public string googleplus_redirect_url = "https://account.google.com";
        public string authorizationEndpoint = "https://accounts.google.com/o/oauth2/v2/auth";
        public string tokenEndpoint = "https://www.googleapis.com/oauth2/v4/token";
        public string userInfoEndpoint = "https://www.googleapis.com/oauth2/v3/userinfo";
        public string scope = "openid email profile";




        /// <summary>
        /// Descarga una imagen
        /// </summary>
        /// <param name="url">Url</param>
        /// <returns>Objeto imagen</returns>
        public Image Descarga(string url)
        {
            Image image = null;
            HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            webRequest.AllowWriteStreamBuffering = true;
            webRequest.Timeout = 30000;

            WebResponse webResponse = webRequest.GetResponse();

            Stream stream = webResponse.GetResponseStream();

            image = Image.FromStream(stream);

            return ResizeImage(image, new Size(100, 100));

        }
        /// <summary>
        /// Modifica el tamaño de una imagen
        /// </summary>
        /// <param name="imgToResize">Imagen</param>
        /// <param name="size">tamaño</param>
        /// <returns>Objeto imagen</returns>
        public Image ResizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }



        public int GetRandomUnusedPort()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            var port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();
            return port;
        }



        public string randomDataBase64url(uint length)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] bytes = new byte[length];
            rng.GetBytes(bytes);
            return base64urlencodeNoPadding(bytes);
        }



        public string base64urlencodeNoPadding(byte[] buffer)
        {
            string base64 = Convert.ToBase64String(buffer);

            // Converts base64 to base64url.
            base64 = base64.Replace("+", "-");
            base64 = base64.Replace("/", "_");
            // Strips padding.
            base64 = base64.Replace("=", "");

            return base64;
        }


        public byte[] sha256(string inputStirng)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(inputStirng);
            SHA256Managed sha256 = new SHA256Managed();
            return sha256.ComputeHash(bytes);
        }

    }
}
