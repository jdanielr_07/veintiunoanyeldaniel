﻿using System;
using veintiuno_utn.DAL;
using veintiuno_utn.Entities;

namespace veintiuno_utn.BOL
{
    public class DeckBOL
    {
        public EMesa mesa { get; set; }
        public DeckDAL Dal()
        {
            return new DeckDAL();
        }
        /// <summary>
        /// Agrega una mesa
        /// </summary>
        /// <param name="m">Mesa que desea agregar</param>
        public void AgregarMesa(EMesa m)
        {
            new DeckDAL().InsertarMesa(m);
        }

        /// <summary>
        /// Valida una mesa
        /// </summary>
        /// <param name="m">Mesa</param>
        private void ValidarMesa(EMesa m)
        {

            if (string.IsNullOrEmpty(m.Codigo_invitacion))
            {
                throw new Exception("Código requerido");
            }
            if (string.IsNullOrEmpty(m.Pass))
            {
                throw new Exception("Contraseña requerida");
            }
        }
    }
}
