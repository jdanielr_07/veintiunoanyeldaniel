﻿using System;
using System.Collections.Generic;
using veintiuno_utn.DAL;
using veintiuno_utn.Entities;

namespace veintiuno_utn.BOL
{
    public class UsuarioBOL
    {
        /// <summary>
        /// Guarda un usuario
        /// </summary>
        /// <param name="u"></param>
        /// <returns></returns> usuario
        public bool Guardar(EUsuario u)
        {
            return new UsuarioDAL().Login(u);
        }
        /// <summary>
        /// Retorna id del usuario
        /// </summary>
        /// <param name="correo">correo del usuario</param> 
        /// <returns>id del usuario</returns> 
        public int RetornarID(string correo)
        {
            return new UsuarioDAL().id(correo);
        }

        /// <summary>
        /// Inicia una partida
        /// </summary>
        /// <param name="ep">Objeto Partida</param>
        public void IniciarPartida(EPartida ep)
        {
            new UsuarioDAL().InsertarPartidaNueva(ep);
        }
        /// <summary>
        /// Consulta el dinero de un usuario
        /// </summary>
        /// <param name="ep">id del usuario </param>
        /// <returns>Dinero disponible</returns>
        public double TraerDinero(int ep)
        {
            return new UsuarioDAL().ConsultarDinero(ep);
        }

        /// <summary>
        /// carga un usuario por medio del id
        /// </summary>
        /// <param name="id">identificación del usuario</param>
        /// <returns>Usuario</returns>
        public EUsuario TraerUsuarios(int id)
        {
            return new UsuarioDAL().CargarUsuarios(id);
        }
        /// <summary>
        /// Inserta una apuesta
        /// </summary>
        /// <param name="id">id del usuario</param>
        /// <param name="id_mesa">id de la </param>
        /// <param name="apuesta">double apuesta</param>
        public void TraerApuesta(int id, int id_mesa, double apuesta)
        {
            new UsuarioDAL().InsertarApuesta(id, id_mesa, apuesta);
          

        }

        /// <summary>
        /// Suma la cantidad dada al saldo actual
        /// </summary>
        /// <param name="dinero">Monto double</param>
        /// <param name="id">id del usuario</param>
        public void Deposito(double dinero, int id)

        {
           
            if(dinero >= 1)
            {
                double saldo = ObtenerSaldo(id);
                double actual = saldo + dinero;
                 new UsuarioDAL().InsertarApuesta(actual, id);

                //throw new Exception("Monto inválido");
            }
            //return true;
           

        }
        /// <summary>
        /// Obtiene el saldo mediante el id
        /// </summary>
        /// <param name="id">id del usuario</param>
        /// <returns>saldo actual</returns>
        private double ObtenerSaldo(int id)
        {
            return new UsuarioDAL().ConsultarDinero(id);
        }
        /// <summary>
        /// Valida el monto de la apuesta
        /// </summary>
        /// <param name="apuesta">Apuesta double</param>
        /// <param name="id">id del usuario</param>
        /// <returns>int</returns>
        public int ValidarApuesta(double apuesta,int id)
        {
            double saldo = ObtenerSaldo(id);
           
            if (apuesta > saldo)
            {
                //throw new Exception("Saldo insuficiente");
                return 2;
            }
            else
            {
                double d = saldo - apuesta;
                new UsuarioDAL().InsertarApuesta(d,id);
                return 3;
            }
            
        }

    }
}

