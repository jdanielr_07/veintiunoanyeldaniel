﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using veintiuno_utn.DAL;
using veintiuno_utn.Entities;

namespace veintiuno_utn.BOL
{
    public class PartidaBOL
    {
        /// <summary>
        /// Obtiene el estado de una partida
        /// </summary>
        /// <param name="p">Partida</param>
        /// <returns>Patida</returns>
        public EPartida EstadoPartida(EPartida p)
        {
            return new PartidaDAL().VerificarTurno(p);
        }
        /// <summary>
        /// Obtiene el  estado del turno
        /// </summary>
        /// <param name="p">Partida</param>
        /// <returns>booleano</returns>
        public bool EstadoTurno(EPartida p)
        {
            return new PartidaDAL().VerificarTurnoJug(p);
        }
        /// <summary>
        /// Obtiene el estado de una apuesta
        /// </summary>
        /// <param name="p">Apuesta</param>
        /// <returns>booleano</returns>
        public bool EstadoApuesta(EApuesta p)
        {
            return new PartidaDAL().VerificarApuesta(p);
        }
        /// <summary>
        /// Cambia el turno
        /// </summary>
        /// <param name="p">Partida</param>
        public void CambioTurno(EPartida p)
        {
            new PartidaDAL().TurnoCambio(p);
        }
        /// <summary>
        /// Aumenta una apuesta
        /// </summary>
        /// <param name="id">int id</param>
        /// <param name="id_mesa">id mesa</param>
        /// <param name="apuesta">double apuesta</param>
        public void AumentarApuesta(int id, int id_mesa, double apuesta)
        {
          

            new PartidaDAL().AumentoApuesta(id, id_mesa, apuesta);
        }
        /// <summary>
        /// inserta las apuesta al ganador
        /// </summary>
        /// <param name="id">id ganador</param>
        /// <param name="apuesta">apuesta</param>
        public void Premio(int id, double apuesta)
        {
            new PartidaDAL().RecibirPremio(id, apuesta);
        }
        /// <summary>
        /// Verfica una apuesta
        /// </summary>
        /// <param name="p">Apuesta</param>
        /// <returns>double</returns>
        public double VerificarApuesta(EApuesta p)
        {
            return new PartidaDAL().ApuestaEnMesa(p);
        }
        /// <summary>
        /// Verifica el invertido
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public double Invertido(EApuesta p)
        {
            return new PartidaDAL().VerificarInvertido(p);
        }
        /// <summary>
        /// Obtiene puntos del ganador
        /// </summary>
        /// <param name="p">Carta</param>
        /// <returns></returns>
        public int TotalForWin(ECarta p)
        {
            return new PartidaDAL().Total(p);
        }
    }
}
