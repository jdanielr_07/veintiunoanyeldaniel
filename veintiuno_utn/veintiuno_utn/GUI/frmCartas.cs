﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;
using veintiuno_utn.DAL;
using veintiuno_utn.Entities;
using veintiuno_utn.BOL;
using WebService;
using System.Windows.Forms.DataVisualization.Charting;

namespace veintiuno_utn.GUI
{
    public partial class FrmCartas : Form
    {
        private DeckDAL ddal;
        private DeckBOL bol;
        private UsuarioBOL Usbol;
        private List<XElement> li;
        private List<string> listaValores;
        private List<string> listaPng;
        private List<string> listaCartasDealer;
        private List<string> listaValoresDealer;
        private List<ECarta> ListaCartas;
        private List<PictureBox> ptbs;
        private List<PictureBox> profiles_jug;
        private List<PictureBox> cartas_mesa;
        private List<PictureBox> cartas;
        private List<PictureBox> cartasDealer;
        private int nuevas_cartas;
        private XDocument xml1;
        private string codigoDeck;
        private int Restantes;
        private EUsuario logeado;
        private MesaBOL Mbol;
        private CartaBOL Cbol;
        private EDecks deck;
        public EPartida part;
        public PartidaBOL Pbol;
        private List<int> totalJug;
        private int dealer;
        private double ganancia;

        private int pos;
        public EMesa mesa { get; set; }
        public FrmCartas(EUsuario e, string tipo)
        {
            InitializeComponent();
            this.logeado = e;
            ganancia = 0;
            totalJug = new List<int>();
            pos = 1;
            dealer = 0;
            listaCartasDealer = new List<string>();
            listaValoresDealer = new List<string>();
            cartasDealer = new List<PictureBox>();
            cartas = new List<PictureBox>();
            Pbol = new PartidaBOL();
            ListaCartas = new List<ECarta>();
            part = new EPartida();
            deck = new EDecks();
            Cbol = new CartaBOL();
            Usbol = new UsuarioBOL();
            ddal = new DeckDAL();
            bol = new DeckBOL();
            li = new List<XElement>();
            listaValores = new List<string>();
            listaPng = new List<string>();
            ptbs = new List<PictureBox>();
            cartas_mesa = new List<PictureBox>();
            profiles_jug = new List<PictureBox>();
            nuevas_cartas = 1;
            Restantes = 0;
            mesa = new EMesa();
            Mbol = new MesaBOL();

        }
        private void frmCartas_Load(object sender, EventArgs e)
        {
            Comienzo();
            AbrirMesa();
            lblNombre.Text = logeado.Nombre;
            ptbProfile.Image = logeado.Foto;
        }
        private int suma(string valores)
        {
            int total = 0;
            if (valores.Equals("QUEEN") || valores.Equals("JACK") || valores.Equals("KING"))
            {
                total = 10;
            }
            else if (valores.Equals("ACE"))
            {
                total = 11;
            }
            else
            {
                total = Convert.ToInt32(valores);
            }
            return total;
        }

        /// <summary>
        /// Agrega picture box a una lista
        /// </summary>
        private void Comienzo()
        {

            ptbs.Add(ptbCarta1);
            ptbs.Add(ptbCarta2);
            ptbs.Add(ptbCarta3);
            ptbs.Add(ptbCarta4);
            ptbs.Add(ptbCarta5);
            ptbs.Add(ptbCarta6);
            ptbs.Add(ptbCarta7);
            ptbs.Add(ptbCarta8);
            ptbs.Add(ptbCarta9);
            ptbs.Add(ptbCarta10);
            ptbs.Add(ptbCarta11);
            ptbs.Add(ptbCarta12);
            ptbs.Add(ptbCarta13);

            profiles_jug.Add(ptbJugador4);
            profiles_jug.Add(ptbJugador3);
            profiles_jug.Add(ptbJugador5);
            profiles_jug.Add(ptbJugador2);
            profiles_jug.Add(ptbJugador6);
            profiles_jug.Add(ptbJugador1);

            cartas_mesa.Add(ptbCarta1Jug4);
            cartas_mesa.Add(ptbCarta2Jug4);
            cartas_mesa.Add(ptbCarta1Jug3);
            cartas_mesa.Add(ptbCarta2Jug3);
            cartas_mesa.Add(ptbCarta1Jug5);
            cartas_mesa.Add(ptbCarta2Jug5);
            cartas_mesa.Add(ptbCarta1Jug2);
            cartas_mesa.Add(ptbCarta2Jug2);
            cartas_mesa.Add(ptbCarta1Jug6);
            cartas_mesa.Add(ptbCarta1Jug6);
            cartas_mesa.Add(ptbCarta2Jug1);
            cartas_mesa.Add(ptbCarta2Jug1);

            cartas.Add(ptbCarta1);
            cartas.Add(ptbCarta2);
            cartas.Add(ptbCarta3);
            cartas.Add(ptbCarta4);
            cartas.Add(ptbCarta5);
            cartas.Add(ptbCarta6);
            cartas.Add(ptbCarta7);
            cartas.Add(ptbCarta8);
            cartas.Add(ptbCarta9);
            cartas.Add(ptbCarta10);
            cartas.Add(ptbCarta11);
            cartas.Add(ptbCarta12);
            cartas.Add(ptbCarta13);

            cartasDealer.Add(ptbDealerCarta6);
            cartasDealer.Add(ptbDealerCarta7);
            cartasDealer.Add(ptbDealerCarta5);
            cartasDealer.Add(ptbDealerCarta8);
            cartasDealer.Add(ptbDealerCarta4);
            cartasDealer.Add(ptbDealerCarta9);
            cartasDealer.Add(ptbDealerCarta3);
            cartasDealer.Add(ptbDealerCarta10);
            cartasDealer.Add(ptbDealerCarta2);
            cartasDealer.Add(ptbDealerCarta11);
            cartasDealer.Add(ptbDealerCarta1);
            cartasDealer.Add(ptbDealerCarta12);
            cartasDealer.Add(ptbDealerCarta13);
            for (int i = 0; i < profiles_jug.Count; i++)
            {
                profiles_jug[i].Parent = ptbMesa;
            }
            lblMess.Parent = ptbMesa;
            ptbDealer.Parent = ptbMesa;
            ptbProfile.Image = logeado.Foto;
            ptbJugador4.Image = logeado.Foto;
            lblNombre.Text = logeado.Nombre;
            ptbDealer.Parent = ptbMesa;
            btnComenzar.Parent = ptbMesa;
            btnApostar.Parent = ptbMesa;
            lblAccciones.Parent = ptbMesa;
        }
        /// <summary>
        /// Inicializa la mesa
        /// </summary>
        public void AbrirMesa()
        {
            Mbol.EstadoMesa(mesa);
            if (mesa.Partida_en_curso == false)
            {
                string id_part = Password.GenerarPassword(8);
                string pass = Password.GenerarPassword(8);
                mesa.Codigo_invitacion = id_part;
                mesa.Pass = Encrypt.Encriptar(pass);
                mesa.Cantidad_max_jug = 6;
                mesa.Jugadores_en_mesa = 0;
                mesa.Partida_en_curso = true;
                bol.AgregarMesa(mesa);
                ddal.CargarMesaActual(mesa);
                int id = ddal.IdMesa(id_part);
                for (int i = 0; i < 6; i++)
                {
                    var json2 = new WebClient().DownloadString("https://deckofcardsapi.com/api/deck/new/draw/?count=0");
                    var test = json2.ParseJSON<Rootobject>();
                    XNode node = JsonConvert.DeserializeXNode(json2, "Root");
                    string XML = node.ToString();
                    xml1 = XDocument.Parse(XML);
                    string deck = xml1.Descendants("deck_id").ToList().Last().Value;
                    string valor = xml1.Descendants("remaining").ToList().Last().Value;
                    int remaining = Int32.Parse(valor);
                    EDecks d = new EDecks()
                    {
                        Codigo = deck,
                        Id_mesa = id,
                        Cartas_restantes = remaining
                    };
                    ddal.InsertarDecks(d);
                }
                deck.Id_mesa = mesa.Id;
                ddal.DeckAUsar(deck);
            }

        }
        /// <summary>
        /// Inicia una partida
        /// </summary>
        public void Iniciar()
        {
            lblMess.Visible = true;
            ptbCarta2.Visible = true;
            ddal.CargarMesaActual(mesa);
            deck.Id_mesa = mesa.Id;
            ddal.DeckAUsar(deck);
            mesa.Jugadores_en_mesa += 1;
            if (mesa.Jugadores_en_mesa == 1)
            {
                EPartida p3 = new EPartida()
                {
                    Num_jugador = mesa.Jugadores_en_mesa,
                    Id_mesa = mesa.Id,
                    Id_usuario = logeado.Id,
                    Turno = true
                };
                Usbol.IniciarPartida(p3);
                part = p3;
                Mbol.ActualizarJugadoresMesa(mesa);
                codigoDeck = deck.Codigo;
                Restantes = deck.Cartas_restantes;
                string draw = String.Format("https://deckofcardsapi.com/api/deck/" + "{0}" + "/draw/?count=2", deck.Codigo);
                var json = new WebClient().DownloadString(draw);
                var test1 = json.ParseJSON<Rootobject>();
                XNode node1 = JsonConvert.DeserializeXNode(json, "Root");
                string xml = node1.ToString();
                xml1 = XDocument.Parse(xml);
                string valor1 = xml1.Descendants("remaining").ToList().Last().Value;
                int valorRem = Int32.Parse(valor1);
                ddal.CantidadCartas(valorRem, deck.Codigo);
                li = xml1.Descendants("png").ToList();
                foreach (var item in li)
                {
                    string dato = item.Value;
                    listaPng.Add(dato);
                }
                li = xml1.Descendants("value").ToList();
                foreach (var item in li)
                {
                    string dato1 = item.Value;
                    listaValores.Add(dato1);
                }
                EPartida p1 = new EPartida()
                {
                    Num_jugador = 0,
                    Id_mesa = mesa.Id,
                    Id_usuario = 0,
                    Turno = false
                };
                Usbol.IniciarPartida(p1);

                ECarta c = new ECarta()
                {
                    Id_jugador = 0,
                    Deck_id = deck.Id,
                    Id_Mesa = mesa.Id
                };
                int k = Cbol.CargarValorTotal(c);
                while (k <= 16)
                {
                    DealerJuega();
                    k = Cbol.CargarValorTotal(c);
                }
                for (int i = 0; i < 2; i++)
                {
                    ECarta c1 = new ECarta()
                    {
                        Carta = String.Format("{0}", listaPng[i]),
                        Id_jugador = logeado.Id,
                        Deck_id = deck.Id,
                        Id_Mesa = mesa.Id,
                        Valor_num = suma(listaValores[i])
                    };
                    Cbol.InsertarCarta(c1);
                    label4.Visible = true;
                    lblTotal.Text = Cbol.CargarValorTotal(c1).ToString();
                    cartas[i].ImageLocation = c1.Carta;
                    cartasDealer[i].Visible = true;
                }
            }
            else
            {
                EPartida p3 = new EPartida()
                {
                    Num_jugador = mesa.Jugadores_en_mesa,
                    Id_mesa = mesa.Id,
                    Id_usuario = logeado.Id,
                    Turno = false
                };
                Usbol.IniciarPartida(p3);
                part = p3;
                Mbol.ActualizarJugadoresMesa(mesa);
                codigoDeck = deck.Codigo;
                Restantes = deck.Cartas_restantes;
                string draw = String.Format("https://deckofcardsapi.com/api/deck/" + "{0}" + "/draw/?count=2", deck.Codigo);
                var json = new WebClient().DownloadString(draw);
                var test1 = json.ParseJSON<Rootobject>();
                XNode node1 = JsonConvert.DeserializeXNode(json, "Root");
                string xml = node1.ToString();
                xml1 = XDocument.Parse(xml);
                string valor1 = xml1.Descendants("remaining").ToList().Last().Value;
                int valorRem = Int32.Parse(valor1);
                ddal.CantidadCartas(valorRem, deck.Codigo);
                li = xml1.Descendants("png").ToList();
                foreach (var item in li)
                {
                    string dato = item.Value;
                    listaPng.Add(dato);
                }
                li = xml1.Descendants("value").ToList();
                foreach (var item in li)
                {
                    string dato1 = item.Value;
                    listaValores.Add(dato1);
                }
                for (int i = 0; i < 2; i++)
                {
                    ECarta c = new ECarta()
                    {
                        Carta = String.Format("{0}", listaPng[i]),
                        Id_jugador = logeado.Id,
                        Deck_id = deck.Id,
                        Id_Mesa = mesa.Id,
                        Valor_num = suma(listaValores[i])
                    };
                    Cbol.InsertarCarta(c);
                    label4.Visible = true;
                    lblTotal.Text = Cbol.CargarValorTotal(c).ToString();
                    cartas[i].ImageLocation = c.Carta;
                    cartasDealer[i].Visible = true;
                }
            }
            if (lblTotal.Text.Equals("21"))
            {
                lblMess.Text = "Black Jack!";
                btnNuevaCarta.Enabled = true;
                btnDoblar.Enabled = true;
            }
            codigoDeck = deck.Codigo;
            Restantes = deck.Cartas_restantes;
            lblMess.Visible = false;
        }
        private void ActualizarMesa()
        {
            ddal.CargarMesaActual(mesa);
            List<int> ids = new List<int>();
            ids = Mbol.UsuariosPartida(mesa);
            List<EUsuario> usuarios = new List<EUsuario>();
            for (int l = 0; l < ids.Count; l++)
            {
                usuarios.Add(Usbol.TraerUsuarios(ids[l]));
                ECarta c = new ECarta()
                {
                    Id_jugador = ids[l],
                    Id_Mesa = mesa.Id
                };
                Cbol.CargarCartasMesa(c);
                ListaCartas.Add(c);
            }
            for (int i = 0; i < ids.Count; i++)
            {
                profiles_jug[i].Image = usuarios[i].Foto;
                profiles_jug[i].Visible = true;
            }
            ECarta c3 = new ECarta()
            {
                Id_jugador = 0,
                Id_Mesa = mesa.Id
            };
            Cbol.CargarCartasMesa(c3);
            Cartas_mesa(mesa.Jugadores_en_mesa);
            ptbDealerCarta6.ImageLocation = c3.Carta;
        }
        private void Cartas_mesa(int jugadores)
        {
            switch (jugadores)
            {
                case 1:
                    for (int i = 0; i < 2; i++)
                    {
                        cartas_mesa[i].Visible = true;
                    }
                    cartas_mesa[0].ImageLocation = ListaCartas[0].Carta;
                    break;
                case 2:
                    for (int i = 0; i < 4; i++)
                    {
                        cartas_mesa[i].Visible = true;
                    }
                    cartas_mesa[0].ImageLocation = ListaCartas[0].Carta;
                    cartas_mesa[2].ImageLocation = ListaCartas[1].Carta;
                    break;
                case 3:
                    for (int i = 0; i < 6; i++)
                    {
                        cartas_mesa[i].Visible = true;
                    }
                    cartas_mesa[0].ImageLocation = ListaCartas[0].Carta;
                    cartas_mesa[2].ImageLocation = ListaCartas[1].Carta;
                    cartas_mesa[4].ImageLocation = ListaCartas[2].Carta;
                    break;
                case 4:
                    for (int i = 0; i < 8; i++)
                    {
                        cartas_mesa[i].Visible = true;
                    }
                    cartas_mesa[0].ImageLocation = ListaCartas[0].Carta;
                    cartas_mesa[2].ImageLocation = ListaCartas[1].Carta;
                    cartas_mesa[4].ImageLocation = ListaCartas[2].Carta;
                    cartas_mesa[6].ImageLocation = ListaCartas[3].Carta;
                    break;
                case 5:
                    for (int i = 0; i < 10; i++)
                    {
                        cartas_mesa[i].Visible = true;
                    }
                    cartas_mesa[0].ImageLocation = ListaCartas[0].Carta;
                    cartas_mesa[2].ImageLocation = ListaCartas[1].Carta;
                    cartas_mesa[4].ImageLocation = ListaCartas[2].Carta;
                    cartas_mesa[6].ImageLocation = ListaCartas[3].Carta;
                    cartas_mesa[8].ImageLocation = ListaCartas[4].Carta;
                    break;
                case 6:
                    for (int i = 0; i < 12; i++)
                    {
                        cartas_mesa[i].Visible = true;
                    }
                    cartas_mesa[0].ImageLocation = ListaCartas[0].Carta;
                    cartas_mesa[2].ImageLocation = ListaCartas[1].Carta;
                    cartas_mesa[4].ImageLocation = ListaCartas[2].Carta;
                    cartas_mesa[6].ImageLocation = ListaCartas[3].Carta;
                    cartas_mesa[8].ImageLocation = ListaCartas[4].Carta;
                    cartas_mesa[10].ImageLocation = ListaCartas[5].Carta;
                    break;
            }
        }
        private void btnInvitacion_Click(object sender, EventArgs e)
        {
            string correo = "utnblackjack@gmail.com";
            string pass = "blackjack1234";
        }
        private void FrmCartas_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (part.Num_jugador == 1)
            {
                mesa.Partida_en_curso = false;
                Mbol.EstadoPartida(mesa);
            }
        }

        private void FrmCartas_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (part.Num_jugador == 1)
            {
               
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ListaCartas.Clear();
            ActualizarMesa();
            EPartida p1 = new EPartida()
            {
                Num_jugador = part.Num_jugador,
                Id_mesa = mesa.Id,
                Id_usuario = logeado.Id,
            };
            Pbol.EstadoPartida(p1);
            if (p1.Turno == true)
            {
                btnNuevaCarta.Enabled = true;
            }
            else
            {
                btnNuevaCarta.Enabled = false;
            }
            EPartida p2 = new EPartida()
            {
                Num_jugador = 0,
                Id_mesa = mesa.Id
            };
            Pbol.EstadoPartida(p2);
            if (p2.Turno == true)
            {
                List<ECarta> cartas = new List<ECarta>();
                ECarta c3 = new ECarta()
                {
                    Id_jugador = 0,
                    Id_Mesa = mesa.Id
                };
                int i = Cbol.CargarValorTotal(c3);
                cartas = Cbol.CargarCartasDealer(c3);
                for (int q = 1; q < cartas.Count; q++)
                {
                    cartasDealer[q].Visible = true;
                    cartasDealer[q].ImageLocation = cartas[q].Carta;
                }
            }
        }
        private void Ganador()
        {
            int valor = 21;
            int closest = totalJug.Aggregate((x, y) => Math.Abs(x - valor) < Math.Abs(y - valor) ? x : y);
            lblMess.Visible = true;
            for (int i = 0; i < totalJug.Count; i++)
            {
                if (totalJug[i] == closest)
                {
                    if (i == 0)
                    {
                        lblMess.Text = "El dealer ha ganado la partida!";
                        if (MessageBox.Show("Desea otra partida?", "Volver a jugar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            this.Close();
                            FrmCartas frm = new FrmCartas(logeado, "1");
                            frm.Show();
                        }
                        else
                        {
                            Close();

                        }
                        mesa.Partida_en_curso = false;
                        Mbol.EstadoPartida(mesa);
                    }
                    else
                    {
                        lblMess.Text = "El jugador número " + i + " ha ganado la partida!";
                        if(part.Id_usuario == logeado.Id)
                        {
                            ganancia = 0;
                            EApuesta ap = new EApuesta()
                            {
                                Id_usuario = logeado.Id,
                                Id_mesa = mesa.Id
                            };
                            Pbol.EstadoApuesta(ap);
                            double dinero = Usbol.TraerDinero(logeado.Id);
                            if (ap.apuesta > 0)
                            {
                                ganancia = dinero + (ap.apuesta * 2);
                                MessageBox.Show(ganancia.ToString());
                                Pbol.Premio(logeado.Id, ganancia);
                                if (MessageBox.Show("Desea otra partida?", "Volver a jugar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    this.Close();
                                    FrmCartas frm = new FrmCartas(logeado, "1");
                                    frm.Show();                                 
                                }
                                else
                                {
                                    Close();
                                }
                                mesa.Partida_en_curso = false;
                                Mbol.EstadoPartida(mesa);
                            }
                        }

                        
                    }
                }
            }
           
        }
        /// <summary>
        /// Metodo para que el dealer juegue
        /// </summary>
        private void DealerJuega()
        {
            string draw = String.Format("https://deckofcardsapi.com/api/deck/" + "{0}" + "/draw/?count=1", codigoDeck);
            var json = new WebClient().DownloadString(draw);
            var test1 = json.ParseJSON<Rootobject>();
            XNode node1 = JsonConvert.DeserializeXNode(json, "Root");
            string xml = node1.ToString();
            xml1 = XDocument.Parse(xml);
            string valor1 = xml1.Descendants("remaining").ToList().Last().Value;
            int valorRem = Int32.Parse(valor1);
            ddal.CantidadCartas(valorRem, codigoDeck);
            li = xml1.Descendants("png").ToList();
            foreach (var item in li)
            {
                string dato = item.Value;
                listaCartasDealer.Add(dato);
            }
            li = xml1.Descendants("value").ToList();
            foreach (var item in li)
            {
                string dato1 = item.Value;
                listaValoresDealer.Add(dato1);
            }
            ECarta c1 = new ECarta()
            {
                Carta = String.Format("{0}", listaCartasDealer[dealer]),
                Id_jugador = 0,
                Deck_id = deck.Id,
                Id_Mesa = mesa.Id,
                Valor_num = suma(listaValoresDealer[dealer])
            };
            Cbol.InsertarCarta(c1);
            label4.Visible = true;
            dealer += 1;
        }
        private void btnNuevaCarta_Click(object sender, EventArgs e)
        {
            try
            {
                int valor = Convert.ToInt32(lblTotal.Text.Trim());
                if(valor < 21)
                {
                    nuevas_cartas += 1;
                    ptbs[nuevas_cartas].Visible = true;
                    pos += 1;
                    string draw = String.Format("https://deckofcardsapi.com/api/deck/" + "{0}" + "/draw/?count=1", codigoDeck);
                    var json = new WebClient().DownloadString(draw);
                    var test1 = json.ParseJSON<Rootobject>();
                    XNode node1 = JsonConvert.DeserializeXNode(json, "Root");
                    string xml = node1.ToString();
                    xml1 = XDocument.Parse(xml);
                    string valor1 = xml1.Descendants("remaining").ToList().Last().Value;
                    int valorRem = Int32.Parse(valor1);
                    ddal.CantidadCartas(valorRem, codigoDeck);
                    li = xml1.Descendants("png").ToList();
                    foreach (var item in li)
                    {
                        string dato = item.Value;
                        listaPng.Add(dato);
                    }
                    cartas[pos].ImageLocation = String.Format("{0}", listaPng[pos]);
                    li = xml1.Descendants("value").ToList();
                    foreach (var item in li)
                    {
                        string dato1 = item.Value;
                        listaValores.Add(dato1);
                    }
                    ECarta c = new ECarta()
                    {
                        Carta = String.Format("{0}", listaPng[pos]),
                        Id_jugador = logeado.Id,
                        Deck_id = deck.Id,
                        Id_Mesa = mesa.Id,
                        Valor_num = suma(listaValores[pos])
                    };
                    Cbol.InsertarCarta(c);
                    Pbol.CambioTurno(part);
                    label4.Visible = true;
                    lblTotal.Text = Cbol.CargarValorTotal(c).ToString();
                    Pbol.EstadoPartida(part);
                    EPartida ep = new EPartida()
                    {
                        Num_jugador = part.Num_jugador,
                        Id_usuario = logeado.Id,
                        Id_mesa = mesa.Id,
                        Turno = false
                    };
                    Pbol.CambioTurno(ep);
                    part = ep;
                    int jugadores = 0;
                    if (part.Num_jugador < mesa.Jugadores_en_mesa)
                    {
                        jugadores = part.Num_jugador + 1;
                        EPartida ep1 = new EPartida()
                        {
                            Num_jugador = jugadores,
                            Id_mesa = mesa.Id,
                            Turno = true
                        };
                        Pbol.CambioTurno(ep1);
                    }
                    else if (part.Num_jugador >= mesa.Jugadores_en_mesa)
                    {
                        EPartida ep1 = new EPartida()
                        {
                            Num_jugador = jugadores,
                            Id_mesa = mesa.Id,
                            Turno = true
                        };
                        Pbol.CambioTurno(ep1);
                    }
                    for (int i = 0; i < mesa.Jugadores_en_mesa + 1; i++)
                    {
                        ECarta car = new ECarta()
                        {
                            Id_jugador = i,
                            Id_Mesa = mesa.Id
                        };
                        totalJug.Add(Pbol.TotalForWin(car));
                    }
                    EPartida p2 = new EPartida()
                    {
                        Num_jugador = 0,
                        Id_mesa = mesa.Id
                    };
                    Pbol.EstadoPartida(p2);
                    if(p2.Turno == true)
                    {
                        Ganador();
                    }
                }              
            }
            catch (Exception)
            {
                MessageBox.Show("No puede pedir mas cartas!");
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            btnNuevaCarta.Visible = false;
            EPartida ep = new EPartida()
            {
                Num_jugador = part.Num_jugador,
                Id_usuario = logeado.Id,
                Id_mesa = mesa.Id,
                Turno = false
            };
            Pbol.CambioTurno(ep);
            part = ep;
            int jugadores = 0;
            if (part.Num_jugador < mesa.Jugadores_en_mesa)
            {
                jugadores = part.Num_jugador + 1;
                EPartida ep1 = new EPartida()
                {
                    Num_jugador = jugadores,
                    Id_mesa = mesa.Id,
                    Turno = true
                };
                Pbol.CambioTurno(ep1);
            }
            else if (part.Num_jugador >= mesa.Jugadores_en_mesa)
            {
                EPartida ep1 = new EPartida()
                {
                    Num_jugador = jugadores,
                    Id_mesa = mesa.Id,
                    Turno = true
                };
                Pbol.CambioTurno(ep1);
            }
            for (int i = 0; i < mesa.Jugadores_en_mesa + 1; i++)
            {
                ECarta car = new ECarta()
                {
                    Id_jugador = i,
                    Id_Mesa = mesa.Id
                };
                totalJug.Add(Pbol.TotalForWin(car));
            }
            EPartida p2 = new EPartida()
            {
                Num_jugador = 0,
                Id_mesa = mesa.Id
            };
            Pbol.EstadoPartida(p2);
            if (p2.Turno == true)
            {
                Ganador();
            }
        }
        private void btnComenzar_Click(object sender, EventArgs e)
        {
            timer1.Start();
            btnApostar.Visible = false;
            btnComenzar.AccessibleName = "Used";
            btnComenzar.Visible = false;
            lblAccciones.Visible = false;
            btnNuevaCarta.Visible = true;
            btnPlantarse.Visible = true;
            btnDoblar.Visible = true;
            lblMess.Text = "A continuación tus cartas...";
            Iniciar();
            EApuesta aP = new EApuesta()
            {
                Id_usuario = logeado.Id,
                Id_mesa = mesa.Id
            };

        }
        /// <summary>
        /// Segundo timer que se activa después de iniciar la partida
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_Tick(object sender, EventArgs e)
        {
            EApuesta aP = new EApuesta()
            {
                Id_usuario = logeado.Id,
                Id_mesa = mesa.Id
            };
            Pbol.VerificarApuesta(aP);
            lblApostado.Text = "$ " + aP.apuesta;
            if (aP.apuesta > 0 && btnComenzar.AccessibleName.Equals("None"))
            {
                btnComenzar.Visible = true;
                lblApostado.Visible = true;
                lblDinero.Visible = true;
            }
            chartDinero.Series.Clear();
            chartDinero.Titles.Clear();
            Pbol.Invertido(aP);
            string[] seriesArray = { "Invertido", "Ganado" };
            double[] pointsArray = { aP.apuesta, ganancia};
            this.chartDinero.Titles.Add("Ingresos");
            for (int i = 0; i < seriesArray.Length; i++)
            {
                Series series = this.chartDinero.Series.Add(seriesArray[i]);
                series.Points.Add(pointsArray[i]);
            }
            chartDinero.Parent = ptbMesa;
            chartDinero.Visible = true;

        }
        private void btnApostar_Click(object sender, EventArgs e)
        {
            FrmApostar frm = new FrmApostar(logeado.Id, mesa.Id, btnComenzar.AccessibleName);
            frm.ShowDialog();
            timer2.Start();
        }

        private void btnDoblar_Click(object sender, EventArgs e)
        {
            double aumento = 0;
            EApuesta ap = new EApuesta()
            {
                Id_usuario = logeado.Id,
                Id_mesa = mesa.Id
            };
            Pbol.EstadoApuesta(ap);
            if (ap.apuesta > 0)
            {
                aumento = ap.apuesta * 2;
                Pbol.AumentarApuesta(logeado.Id, mesa.Id, aumento);
            }
        }

        private void PtbMesa_Click(object sender, EventArgs e)
        {

        }
    }
}
