﻿namespace veintiuno_utn.GUI
{
    partial class FrmCartas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCartas));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.label5 = new System.Windows.Forms.Label();
            this.lblMess = new System.Windows.Forms.Label();
            this.panelJugador = new System.Windows.Forms.Panel();
            this.lblApostado = new System.Windows.Forms.Label();
            this.lblDinero = new System.Windows.Forms.Label();
            this.btnDoblar = new System.Windows.Forms.PictureBox();
            this.btnNuevaCarta = new System.Windows.Forms.PictureBox();
            this.btnPlantarse = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ptbProfile = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.ptbCarta9 = new System.Windows.Forms.PictureBox();
            this.ptbCarta8 = new System.Windows.Forms.PictureBox();
            this.ptbCarta13 = new System.Windows.Forms.PictureBox();
            this.ptbCarta12 = new System.Windows.Forms.PictureBox();
            this.ptbCarta11 = new System.Windows.Forms.PictureBox();
            this.ptbCarta10 = new System.Windows.Forms.PictureBox();
            this.ptbCarta7 = new System.Windows.Forms.PictureBox();
            this.ptbCarta6 = new System.Windows.Forms.PictureBox();
            this.ptbCarta5 = new System.Windows.Forms.PictureBox();
            this.ptbCarta4 = new System.Windows.Forms.PictureBox();
            this.ptbCarta3 = new System.Windows.Forms.PictureBox();
            this.ptbCarta2 = new System.Windows.Forms.PictureBox();
            this.ptbCarta1 = new System.Windows.Forms.PictureBox();
            this.btnApostar = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta5 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta1 = new System.Windows.Forms.PictureBox();
            this.ptbCarta2Jug6 = new System.Windows.Forms.PictureBox();
            this.ptbCarta1Jug6 = new System.Windows.Forms.PictureBox();
            this.ptbCarta2Jug5 = new System.Windows.Forms.PictureBox();
            this.ptbCarta1Jug5 = new System.Windows.Forms.PictureBox();
            this.ptbCarta2Jug4 = new System.Windows.Forms.PictureBox();
            this.ptbCarta1Jug4 = new System.Windows.Forms.PictureBox();
            this.ptbCarta2Jug3 = new System.Windows.Forms.PictureBox();
            this.ptbCarta1Jug3 = new System.Windows.Forms.PictureBox();
            this.ptbCarta2Jug2 = new System.Windows.Forms.PictureBox();
            this.ptbCarta1Jug2 = new System.Windows.Forms.PictureBox();
            this.ptbCarta2Jug1 = new System.Windows.Forms.PictureBox();
            this.ptbCarta1Jug1 = new System.Windows.Forms.PictureBox();
            this.ptbDealer = new System.Windows.Forms.PictureBox();
            this.ptbJugador1 = new System.Windows.Forms.PictureBox();
            this.ptbJugador2 = new System.Windows.Forms.PictureBox();
            this.ptbJugador3 = new System.Windows.Forms.PictureBox();
            this.ptbJugador5 = new System.Windows.Forms.PictureBox();
            this.ptbJugador6 = new System.Windows.Forms.PictureBox();
            this.ptbJugador4 = new System.Windows.Forms.PictureBox();
            this.ptbMesa = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta7 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta8 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta9 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta10 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta11 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta12 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta13 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta6 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta3 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta4 = new System.Windows.Forms.PictureBox();
            this.ptbDealerCarta2 = new System.Windows.Forms.PictureBox();
            this.btnComenzar = new System.Windows.Forms.PictureBox();
            this.lblAccciones = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.chartDinero = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panelJugador.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDoblar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNuevaCarta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPlantarse)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnApostar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbMesa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnComenzar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartDinero)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(402, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 17);
            this.label5.TabIndex = 26;
            // 
            // lblMess
            // 
            this.lblMess.AutoSize = true;
            this.lblMess.BackColor = System.Drawing.Color.Transparent;
            this.lblMess.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMess.Location = new System.Drawing.Point(42, 515);
            this.lblMess.Name = "lblMess";
            this.lblMess.Size = new System.Drawing.Size(190, 37);
            this.lblMess.TabIndex = 25;
            this.lblMess.Text = "Bienvenido!";
            // 
            // panelJugador
            // 
            this.panelJugador.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panelJugador.BackgroundImage = global::veintiuno_utn.Properties.Resources.Fondo_jugador;
            this.panelJugador.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelJugador.Controls.Add(this.lblApostado);
            this.panelJugador.Controls.Add(this.lblDinero);
            this.panelJugador.Controls.Add(this.btnDoblar);
            this.panelJugador.Controls.Add(this.btnNuevaCarta);
            this.panelJugador.Controls.Add(this.btnPlantarse);
            this.panelJugador.Controls.Add(this.groupBox1);
            this.panelJugador.Controls.Add(this.label4);
            this.panelJugador.Controls.Add(this.lblTotal);
            this.panelJugador.Controls.Add(this.ptbCarta9);
            this.panelJugador.Controls.Add(this.ptbCarta8);
            this.panelJugador.Controls.Add(this.ptbCarta13);
            this.panelJugador.Controls.Add(this.ptbCarta12);
            this.panelJugador.Controls.Add(this.ptbCarta11);
            this.panelJugador.Controls.Add(this.ptbCarta10);
            this.panelJugador.Controls.Add(this.ptbCarta7);
            this.panelJugador.Controls.Add(this.ptbCarta6);
            this.panelJugador.Controls.Add(this.ptbCarta5);
            this.panelJugador.Controls.Add(this.ptbCarta4);
            this.panelJugador.Controls.Add(this.ptbCarta3);
            this.panelJugador.Controls.Add(this.ptbCarta2);
            this.panelJugador.Controls.Add(this.ptbCarta1);
            this.panelJugador.Location = new System.Drawing.Point(-4, 578);
            this.panelJugador.Name = "panelJugador";
            this.panelJugador.Size = new System.Drawing.Size(2151, 499);
            this.panelJugador.TabIndex = 14;
            // 
            // lblApostado
            // 
            this.lblApostado.BackColor = System.Drawing.Color.Transparent;
            this.lblApostado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblApostado.Font = new System.Drawing.Font("Century Gothic", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApostado.ForeColor = System.Drawing.SystemColors.Control;
            this.lblApostado.Location = new System.Drawing.Point(1718, 269);
            this.lblApostado.Name = "lblApostado";
            this.lblApostado.Size = new System.Drawing.Size(335, 103);
            this.lblApostado.TabIndex = 47;
            // 
            // lblDinero
            // 
            this.lblDinero.AutoSize = true;
            this.lblDinero.BackColor = System.Drawing.Color.Transparent;
            this.lblDinero.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDinero.Location = new System.Drawing.Point(1730, 227);
            this.lblDinero.Name = "lblDinero";
            this.lblDinero.Size = new System.Drawing.Size(169, 22);
            this.lblDinero.TabIndex = 46;
            this.lblDinero.Text = "Dinero apostado:";
            this.lblDinero.Visible = false;
            // 
            // btnDoblar
            // 
            this.btnDoblar.BackColor = System.Drawing.Color.Transparent;
            this.btnDoblar.Image = global::veintiuno_utn.Properties.Resources.Doblar;
            this.btnDoblar.Location = new System.Drawing.Point(1823, 8);
            this.btnDoblar.Name = "btnDoblar";
            this.btnDoblar.Size = new System.Drawing.Size(115, 107);
            this.btnDoblar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnDoblar.TabIndex = 45;
            this.btnDoblar.TabStop = false;
            this.btnDoblar.Visible = false;
            this.btnDoblar.Click += new System.EventHandler(this.btnDoblar_Click);
            // 
            // btnNuevaCarta
            // 
            this.btnNuevaCarta.BackColor = System.Drawing.Color.Transparent;
            this.btnNuevaCarta.Image = global::veintiuno_utn.Properties.Resources.Nueva_Carta;
            this.btnNuevaCarta.Location = new System.Drawing.Point(1567, 8);
            this.btnNuevaCarta.Name = "btnNuevaCarta";
            this.btnNuevaCarta.Size = new System.Drawing.Size(117, 107);
            this.btnNuevaCarta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnNuevaCarta.TabIndex = 44;
            this.btnNuevaCarta.TabStop = false;
            this.btnNuevaCarta.Visible = false;
            this.btnNuevaCarta.Click += new System.EventHandler(this.btnNuevaCarta_Click);
            // 
            // btnPlantarse
            // 
            this.btnPlantarse.BackColor = System.Drawing.Color.Transparent;
            this.btnPlantarse.Image = global::veintiuno_utn.Properties.Resources.Plantarse;
            this.btnPlantarse.Location = new System.Drawing.Point(1704, 8);
            this.btnPlantarse.Name = "btnPlantarse";
            this.btnPlantarse.Size = new System.Drawing.Size(112, 106);
            this.btnPlantarse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnPlantarse.TabIndex = 43;
            this.btnPlantarse.TabStop = false;
            this.btnPlantarse.Visible = false;
            this.btnPlantarse.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.lblNombre);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ptbProfile);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(14, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(344, 392);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Perfil del jugador";
            // 
            // lblNombre
            // 
            this.lblNombre.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblNombre.Location = new System.Drawing.Point(11, 283);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(315, 61);
            this.lblNombre.TabIndex = 39;
            this.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(260, 30);
            this.label3.TabIndex = 38;
            this.label3.Text = "Nombre del jugador:";
            // 
            // ptbProfile
            // 
            this.ptbProfile.Location = new System.Drawing.Point(37, 31);
            this.ptbProfile.Name = "ptbProfile";
            this.ptbProfile.Size = new System.Drawing.Size(236, 199);
            this.ptbProfile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ptbProfile.TabIndex = 37;
            this.ptbProfile.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1415, 227);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(264, 22);
            this.label4.TabIndex = 40;
            this.label4.Text = "Valor de tus cartas en total:";
            this.label4.Visible = false;
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTotal.Font = new System.Drawing.Font("Century Gothic", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.SystemColors.Control;
            this.lblTotal.Location = new System.Drawing.Point(1446, 269);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(198, 103);
            this.lblTotal.TabIndex = 39;
            // 
            // ptbCarta9
            // 
            this.ptbCarta9.AccessibleDescription = "";
            this.ptbCarta9.AccessibleName = "8";
            this.ptbCarta9.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta9.Location = new System.Drawing.Point(541, 216);
            this.ptbCarta9.Name = "ptbCarta9";
            this.ptbCarta9.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta9.TabIndex = 26;
            this.ptbCarta9.TabStop = false;
            this.ptbCarta9.Visible = false;
            // 
            // ptbCarta8
            // 
            this.ptbCarta8.AccessibleDescription = "";
            this.ptbCarta8.AccessibleName = "7";
            this.ptbCarta8.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta8.Location = new System.Drawing.Point(370, 216);
            this.ptbCarta8.Name = "ptbCarta8";
            this.ptbCarta8.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta8.TabIndex = 25;
            this.ptbCarta8.TabStop = false;
            this.ptbCarta8.Visible = false;
            // 
            // ptbCarta13
            // 
            this.ptbCarta13.AccessibleDescription = "";
            this.ptbCarta13.AccessibleName = "12";
            this.ptbCarta13.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta13.Location = new System.Drawing.Point(1213, 216);
            this.ptbCarta13.Name = "ptbCarta13";
            this.ptbCarta13.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta13.TabIndex = 23;
            this.ptbCarta13.TabStop = false;
            this.ptbCarta13.Visible = false;
            // 
            // ptbCarta12
            // 
            this.ptbCarta12.AccessibleDescription = "";
            this.ptbCarta12.AccessibleName = "11";
            this.ptbCarta12.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta12.Location = new System.Drawing.Point(1038, 216);
            this.ptbCarta12.Name = "ptbCarta12";
            this.ptbCarta12.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta12.TabIndex = 22;
            this.ptbCarta12.TabStop = false;
            this.ptbCarta12.Visible = false;
            // 
            // ptbCarta11
            // 
            this.ptbCarta11.AccessibleDescription = "";
            this.ptbCarta11.AccessibleName = "10";
            this.ptbCarta11.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta11.Location = new System.Drawing.Point(874, 216);
            this.ptbCarta11.Name = "ptbCarta11";
            this.ptbCarta11.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta11.TabIndex = 21;
            this.ptbCarta11.TabStop = false;
            this.ptbCarta11.Visible = false;
            // 
            // ptbCarta10
            // 
            this.ptbCarta10.AccessibleDescription = "";
            this.ptbCarta10.AccessibleName = "9";
            this.ptbCarta10.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta10.Location = new System.Drawing.Point(706, 216);
            this.ptbCarta10.Name = "ptbCarta10";
            this.ptbCarta10.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta10.TabIndex = 20;
            this.ptbCarta10.TabStop = false;
            this.ptbCarta10.Visible = false;
            // 
            // ptbCarta7
            // 
            this.ptbCarta7.AccessibleDescription = "";
            this.ptbCarta7.AccessibleName = "6";
            this.ptbCarta7.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta7.Location = new System.Drawing.Point(1383, 15);
            this.ptbCarta7.Name = "ptbCarta7";
            this.ptbCarta7.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta7.TabIndex = 19;
            this.ptbCarta7.TabStop = false;
            this.ptbCarta7.Visible = false;
            // 
            // ptbCarta6
            // 
            this.ptbCarta6.AccessibleDescription = "";
            this.ptbCarta6.AccessibleName = "5";
            this.ptbCarta6.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta6.Location = new System.Drawing.Point(1213, 15);
            this.ptbCarta6.Name = "ptbCarta6";
            this.ptbCarta6.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta6.TabIndex = 18;
            this.ptbCarta6.TabStop = false;
            this.ptbCarta6.Visible = false;
            // 
            // ptbCarta5
            // 
            this.ptbCarta5.AccessibleDescription = "";
            this.ptbCarta5.AccessibleName = "4";
            this.ptbCarta5.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta5.Location = new System.Drawing.Point(1038, 15);
            this.ptbCarta5.Name = "ptbCarta5";
            this.ptbCarta5.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta5.TabIndex = 17;
            this.ptbCarta5.TabStop = false;
            this.ptbCarta5.Visible = false;
            // 
            // ptbCarta4
            // 
            this.ptbCarta4.AccessibleDescription = "";
            this.ptbCarta4.AccessibleName = "3";
            this.ptbCarta4.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta4.Location = new System.Drawing.Point(874, 15);
            this.ptbCarta4.Name = "ptbCarta4";
            this.ptbCarta4.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta4.TabIndex = 16;
            this.ptbCarta4.TabStop = false;
            this.ptbCarta4.Visible = false;
            // 
            // ptbCarta3
            // 
            this.ptbCarta3.AccessibleDescription = "";
            this.ptbCarta3.AccessibleName = "2";
            this.ptbCarta3.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta3.Location = new System.Drawing.Point(706, 15);
            this.ptbCarta3.Name = "ptbCarta3";
            this.ptbCarta3.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta3.TabIndex = 15;
            this.ptbCarta3.TabStop = false;
            this.ptbCarta3.Visible = false;
            // 
            // ptbCarta2
            // 
            this.ptbCarta2.AccessibleDescription = "";
            this.ptbCarta2.AccessibleName = "1";
            this.ptbCarta2.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo;
            this.ptbCarta2.Location = new System.Drawing.Point(541, 15);
            this.ptbCarta2.Name = "ptbCarta2";
            this.ptbCarta2.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta2.TabIndex = 2;
            this.ptbCarta2.TabStop = false;
            this.ptbCarta2.Visible = false;
            // 
            // ptbCarta1
            // 
            this.ptbCarta1.AccessibleName = "0";
            this.ptbCarta1.BackColor = System.Drawing.Color.Transparent;
            this.ptbCarta1.InitialImage = global::veintiuno_utn.Properties.Resources.carta_boca_abajo1;
            this.ptbCarta1.Location = new System.Drawing.Point(370, 15);
            this.ptbCarta1.Name = "ptbCarta1";
            this.ptbCarta1.Size = new System.Drawing.Size(146, 184);
            this.ptbCarta1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ptbCarta1.TabIndex = 0;
            this.ptbCarta1.TabStop = false;
            // 
            // btnApostar
            // 
            this.btnApostar.BackColor = System.Drawing.Color.Transparent;
            this.btnApostar.Image = global::veintiuno_utn.Properties.Resources.Money;
            this.btnApostar.Location = new System.Drawing.Point(1821, 427);
            this.btnApostar.Name = "btnApostar";
            this.btnApostar.Size = new System.Drawing.Size(160, 155);
            this.btnApostar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnApostar.TabIndex = 42;
            this.btnApostar.TabStop = false;
            this.btnApostar.Click += new System.EventHandler(this.btnApostar_Click);
            // 
            // ptbDealerCarta5
            // 
            this.ptbDealerCarta5.AccessibleName = "2";
            this.ptbDealerCarta5.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta5.Image")));
            this.ptbDealerCarta5.Location = new System.Drawing.Point(866, 154);
            this.ptbDealerCarta5.Name = "ptbDealerCarta5";
            this.ptbDealerCarta5.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta5.TabIndex = 50;
            this.ptbDealerCarta5.TabStop = false;
            this.ptbDealerCarta5.Visible = false;
            // 
            // ptbDealerCarta1
            // 
            this.ptbDealerCarta1.AccessibleName = "10";
            this.ptbDealerCarta1.Image = global::veintiuno_utn.Properties.Resources.carta_boca_abajo1;
            this.ptbDealerCarta1.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta1.InitialImage")));
            this.ptbDealerCarta1.Location = new System.Drawing.Point(550, 25);
            this.ptbDealerCarta1.Name = "ptbDealerCarta1";
            this.ptbDealerCarta1.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta1.TabIndex = 49;
            this.ptbDealerCarta1.TabStop = false;
            this.ptbDealerCarta1.Visible = false;
            // 
            // ptbCarta2Jug6
            // 
            this.ptbCarta2Jug6.Image = ((System.Drawing.Image)(resources.GetObject("ptbCarta2Jug6.Image")));
            this.ptbCarta2Jug6.Location = new System.Drawing.Point(1683, 172);
            this.ptbCarta2Jug6.Name = "ptbCarta2Jug6";
            this.ptbCarta2Jug6.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta2Jug6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta2Jug6.TabIndex = 48;
            this.ptbCarta2Jug6.TabStop = false;
            this.ptbCarta2Jug6.Visible = false;
            // 
            // ptbCarta1Jug6
            // 
            this.ptbCarta1Jug6.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta1Jug6.InitialImage")));
            this.ptbCarta1Jug6.Location = new System.Drawing.Point(1604, 172);
            this.ptbCarta1Jug6.Name = "ptbCarta1Jug6";
            this.ptbCarta1Jug6.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta1Jug6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta1Jug6.TabIndex = 47;
            this.ptbCarta1Jug6.TabStop = false;
            this.ptbCarta1Jug6.Visible = false;
            // 
            // ptbCarta2Jug5
            // 
            this.ptbCarta2Jug5.Image = ((System.Drawing.Image)(resources.GetObject("ptbCarta2Jug5.Image")));
            this.ptbCarta2Jug5.Location = new System.Drawing.Point(1460, 252);
            this.ptbCarta2Jug5.Name = "ptbCarta2Jug5";
            this.ptbCarta2Jug5.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta2Jug5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta2Jug5.TabIndex = 46;
            this.ptbCarta2Jug5.TabStop = false;
            this.ptbCarta2Jug5.Visible = false;
            // 
            // ptbCarta1Jug5
            // 
            this.ptbCarta1Jug5.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta1Jug5.InitialImage")));
            this.ptbCarta1Jug5.Location = new System.Drawing.Point(1381, 252);
            this.ptbCarta1Jug5.Name = "ptbCarta1Jug5";
            this.ptbCarta1Jug5.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta1Jug5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta1Jug5.TabIndex = 45;
            this.ptbCarta1Jug5.TabStop = false;
            this.ptbCarta1Jug5.Visible = false;
            // 
            // ptbCarta2Jug4
            // 
            this.ptbCarta2Jug4.Image = ((System.Drawing.Image)(resources.GetObject("ptbCarta2Jug4.Image")));
            this.ptbCarta2Jug4.Location = new System.Drawing.Point(1170, 305);
            this.ptbCarta2Jug4.Name = "ptbCarta2Jug4";
            this.ptbCarta2Jug4.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta2Jug4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta2Jug4.TabIndex = 44;
            this.ptbCarta2Jug4.TabStop = false;
            this.ptbCarta2Jug4.Visible = false;
            // 
            // ptbCarta1Jug4
            // 
            this.ptbCarta1Jug4.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta1Jug4.InitialImage")));
            this.ptbCarta1Jug4.Location = new System.Drawing.Point(1091, 305);
            this.ptbCarta1Jug4.Name = "ptbCarta1Jug4";
            this.ptbCarta1Jug4.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta1Jug4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta1Jug4.TabIndex = 43;
            this.ptbCarta1Jug4.TabStop = false;
            this.ptbCarta1Jug4.Visible = false;
            // 
            // ptbCarta2Jug3
            // 
            this.ptbCarta2Jug3.Image = ((System.Drawing.Image)(resources.GetObject("ptbCarta2Jug3.Image")));
            this.ptbCarta2Jug3.Location = new System.Drawing.Point(856, 298);
            this.ptbCarta2Jug3.Name = "ptbCarta2Jug3";
            this.ptbCarta2Jug3.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta2Jug3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta2Jug3.TabIndex = 42;
            this.ptbCarta2Jug3.TabStop = false;
            this.ptbCarta2Jug3.Visible = false;
            // 
            // ptbCarta1Jug3
            // 
            this.ptbCarta1Jug3.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta1Jug3.InitialImage")));
            this.ptbCarta1Jug3.Location = new System.Drawing.Point(777, 298);
            this.ptbCarta1Jug3.Name = "ptbCarta1Jug3";
            this.ptbCarta1Jug3.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta1Jug3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta1Jug3.TabIndex = 41;
            this.ptbCarta1Jug3.TabStop = false;
            this.ptbCarta1Jug3.Visible = false;
            // 
            // ptbCarta2Jug2
            // 
            this.ptbCarta2Jug2.Image = ((System.Drawing.Image)(resources.GetObject("ptbCarta2Jug2.Image")));
            this.ptbCarta2Jug2.Location = new System.Drawing.Point(586, 242);
            this.ptbCarta2Jug2.Name = "ptbCarta2Jug2";
            this.ptbCarta2Jug2.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta2Jug2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta2Jug2.TabIndex = 40;
            this.ptbCarta2Jug2.TabStop = false;
            this.ptbCarta2Jug2.Visible = false;
            // 
            // ptbCarta1Jug2
            // 
            this.ptbCarta1Jug2.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta1Jug2.InitialImage")));
            this.ptbCarta1Jug2.Location = new System.Drawing.Point(507, 242);
            this.ptbCarta1Jug2.Name = "ptbCarta1Jug2";
            this.ptbCarta1Jug2.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta1Jug2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta1Jug2.TabIndex = 39;
            this.ptbCarta1Jug2.TabStop = false;
            this.ptbCarta1Jug2.Visible = false;
            // 
            // ptbCarta2Jug1
            // 
            this.ptbCarta2Jug1.Image = ((System.Drawing.Image)(resources.GetObject("ptbCarta2Jug1.Image")));
            this.ptbCarta2Jug1.Location = new System.Drawing.Point(394, 138);
            this.ptbCarta2Jug1.Name = "ptbCarta2Jug1";
            this.ptbCarta2Jug1.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta2Jug1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta2Jug1.TabIndex = 38;
            this.ptbCarta2Jug1.TabStop = false;
            this.ptbCarta2Jug1.Visible = false;
            // 
            // ptbCarta1Jug1
            // 
            this.ptbCarta1Jug1.InitialImage = ((System.Drawing.Image)(resources.GetObject("ptbCarta1Jug1.InitialImage")));
            this.ptbCarta1Jug1.Location = new System.Drawing.Point(315, 138);
            this.ptbCarta1Jug1.Name = "ptbCarta1Jug1";
            this.ptbCarta1Jug1.Size = new System.Drawing.Size(73, 98);
            this.ptbCarta1Jug1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCarta1Jug1.TabIndex = 37;
            this.ptbCarta1Jug1.TabStop = false;
            this.ptbCarta1Jug1.Visible = false;
            // 
            // ptbDealer
            // 
            this.ptbDealer.BackColor = System.Drawing.Color.Transparent;
            this.ptbDealer.Image = global::veintiuno_utn.Properties.Resources.comerciante__1_;
            this.ptbDealer.Location = new System.Drawing.Point(995, 10);
            this.ptbDealer.Name = "ptbDealer";
            this.ptbDealer.Size = new System.Drawing.Size(120, 113);
            this.ptbDealer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealer.TabIndex = 36;
            this.ptbDealer.TabStop = false;
            // 
            // ptbJugador1
            // 
            this.ptbJugador1.BackColor = System.Drawing.Color.Transparent;
            this.ptbJugador1.Image = ((System.Drawing.Image)(resources.GetObject("ptbJugador1.Image")));
            this.ptbJugador1.Location = new System.Drawing.Point(258, 226);
            this.ptbJugador1.Name = "ptbJugador1";
            this.ptbJugador1.Size = new System.Drawing.Size(89, 79);
            this.ptbJugador1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbJugador1.TabIndex = 35;
            this.ptbJugador1.TabStop = false;
            this.ptbJugador1.Visible = false;
            // 
            // ptbJugador2
            // 
            this.ptbJugador2.BackColor = System.Drawing.Color.Transparent;
            this.ptbJugador2.Image = ((System.Drawing.Image)(resources.GetObject("ptbJugador2.Image")));
            this.ptbJugador2.Location = new System.Drawing.Point(453, 346);
            this.ptbJugador2.Name = "ptbJugador2";
            this.ptbJugador2.Size = new System.Drawing.Size(89, 79);
            this.ptbJugador2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbJugador2.TabIndex = 34;
            this.ptbJugador2.TabStop = false;
            this.ptbJugador2.Visible = false;
            // 
            // ptbJugador3
            // 
            this.ptbJugador3.BackColor = System.Drawing.Color.Transparent;
            this.ptbJugador3.Image = ((System.Drawing.Image)(resources.GetObject("ptbJugador3.Image")));
            this.ptbJugador3.Location = new System.Drawing.Point(777, 422);
            this.ptbJugador3.Name = "ptbJugador3";
            this.ptbJugador3.Size = new System.Drawing.Size(89, 79);
            this.ptbJugador3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbJugador3.TabIndex = 33;
            this.ptbJugador3.TabStop = false;
            this.ptbJugador3.Visible = false;
            // 
            // ptbJugador5
            // 
            this.ptbJugador5.BackColor = System.Drawing.Color.Transparent;
            this.ptbJugador5.Image = ((System.Drawing.Image)(resources.GetObject("ptbJugador5.Image")));
            this.ptbJugador5.Location = new System.Drawing.Point(1518, 368);
            this.ptbJugador5.Name = "ptbJugador5";
            this.ptbJugador5.Size = new System.Drawing.Size(89, 79);
            this.ptbJugador5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbJugador5.TabIndex = 31;
            this.ptbJugador5.TabStop = false;
            this.ptbJugador5.Visible = false;
            // 
            // ptbJugador6
            // 
            this.ptbJugador6.BackColor = System.Drawing.Color.Transparent;
            this.ptbJugador6.Image = ((System.Drawing.Image)(resources.GetObject("ptbJugador6.Image")));
            this.ptbJugador6.Location = new System.Drawing.Point(1752, 252);
            this.ptbJugador6.Name = "ptbJugador6";
            this.ptbJugador6.Size = new System.Drawing.Size(89, 79);
            this.ptbJugador6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbJugador6.TabIndex = 30;
            this.ptbJugador6.TabStop = false;
            this.ptbJugador6.Visible = false;
            // 
            // ptbJugador4
            // 
            this.ptbJugador4.BackColor = System.Drawing.Color.Transparent;
            this.ptbJugador4.Image = ((System.Drawing.Image)(resources.GetObject("ptbJugador4.Image")));
            this.ptbJugador4.Location = new System.Drawing.Point(1197, 422);
            this.ptbJugador4.Name = "ptbJugador4";
            this.ptbJugador4.Size = new System.Drawing.Size(89, 79);
            this.ptbJugador4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbJugador4.TabIndex = 29;
            this.ptbJugador4.TabStop = false;
            this.ptbJugador4.Visible = false;
            // 
            // ptbMesa
            // 
            this.ptbMesa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbMesa.Image = global::veintiuno_utn.Properties.Resources.nueva_mesa;
            this.ptbMesa.Location = new System.Drawing.Point(-28, -16);
            this.ptbMesa.Name = "ptbMesa";
            this.ptbMesa.Size = new System.Drawing.Size(2197, 588);
            this.ptbMesa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbMesa.TabIndex = 18;
            this.ptbMesa.TabStop = false;
            this.ptbMesa.Click += new System.EventHandler(this.PtbMesa_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gray;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::veintiuno_utn.Properties.Resources.borde;
            this.pictureBox1.Location = new System.Drawing.Point(-152, 568);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(2223, 14);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // ptbDealerCarta7
            // 
            this.ptbDealerCarta7.AccessibleName = "1";
            this.ptbDealerCarta7.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta7.Image")));
            this.ptbDealerCarta7.Location = new System.Drawing.Point(1024, 163);
            this.ptbDealerCarta7.Name = "ptbDealerCarta7";
            this.ptbDealerCarta7.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta7.TabIndex = 52;
            this.ptbDealerCarta7.TabStop = false;
            this.ptbDealerCarta7.Visible = false;
            // 
            // ptbDealerCarta8
            // 
            this.ptbDealerCarta8.AccessibleName = "3";
            this.ptbDealerCarta8.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta8.Image")));
            this.ptbDealerCarta8.Location = new System.Drawing.Point(1104, 154);
            this.ptbDealerCarta8.Name = "ptbDealerCarta8";
            this.ptbDealerCarta8.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta8.TabIndex = 53;
            this.ptbDealerCarta8.TabStop = false;
            this.ptbDealerCarta8.Visible = false;
            // 
            // ptbDealerCarta9
            // 
            this.ptbDealerCarta9.AccessibleName = "5";
            this.ptbDealerCarta9.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta9.Image")));
            this.ptbDealerCarta9.Location = new System.Drawing.Point(1183, 138);
            this.ptbDealerCarta9.Name = "ptbDealerCarta9";
            this.ptbDealerCarta9.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta9.TabIndex = 54;
            this.ptbDealerCarta9.TabStop = false;
            this.ptbDealerCarta9.Visible = false;
            // 
            // ptbDealerCarta10
            // 
            this.ptbDealerCarta10.AccessibleName = "7";
            this.ptbDealerCarta10.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta10.Image")));
            this.ptbDealerCarta10.Location = new System.Drawing.Point(1262, 116);
            this.ptbDealerCarta10.Name = "ptbDealerCarta10";
            this.ptbDealerCarta10.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta10.TabIndex = 55;
            this.ptbDealerCarta10.TabStop = false;
            this.ptbDealerCarta10.Visible = false;
            // 
            // ptbDealerCarta11
            // 
            this.ptbDealerCarta11.AccessibleName = "9";
            this.ptbDealerCarta11.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta11.Image")));
            this.ptbDealerCarta11.Location = new System.Drawing.Point(1341, 84);
            this.ptbDealerCarta11.Name = "ptbDealerCarta11";
            this.ptbDealerCarta11.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta11.TabIndex = 56;
            this.ptbDealerCarta11.TabStop = false;
            this.ptbDealerCarta11.Visible = false;
            // 
            // ptbDealerCarta12
            // 
            this.ptbDealerCarta12.AccessibleName = "11";
            this.ptbDealerCarta12.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta12.Image")));
            this.ptbDealerCarta12.Location = new System.Drawing.Point(1420, 44);
            this.ptbDealerCarta12.Name = "ptbDealerCarta12";
            this.ptbDealerCarta12.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta12.TabIndex = 57;
            this.ptbDealerCarta12.TabStop = false;
            this.ptbDealerCarta12.Visible = false;
            // 
            // ptbDealerCarta13
            // 
            this.ptbDealerCarta13.AccessibleName = "12";
            this.ptbDealerCarta13.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta13.Image")));
            this.ptbDealerCarta13.Location = new System.Drawing.Point(1499, 25);
            this.ptbDealerCarta13.Name = "ptbDealerCarta13";
            this.ptbDealerCarta13.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta13.TabIndex = 58;
            this.ptbDealerCarta13.TabStop = false;
            this.ptbDealerCarta13.Visible = false;
            // 
            // ptbDealerCarta6
            // 
            this.ptbDealerCarta6.AccessibleName = "0";
            this.ptbDealerCarta6.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta6.Image")));
            this.ptbDealerCarta6.Location = new System.Drawing.Point(945, 163);
            this.ptbDealerCarta6.Name = "ptbDealerCarta6";
            this.ptbDealerCarta6.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta6.TabIndex = 59;
            this.ptbDealerCarta6.TabStop = false;
            this.ptbDealerCarta6.Visible = false;
            // 
            // ptbDealerCarta3
            // 
            this.ptbDealerCarta3.AccessibleName = "6";
            this.ptbDealerCarta3.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta3.Image")));
            this.ptbDealerCarta3.Location = new System.Drawing.Point(708, 102);
            this.ptbDealerCarta3.Name = "ptbDealerCarta3";
            this.ptbDealerCarta3.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta3.TabIndex = 60;
            this.ptbDealerCarta3.TabStop = false;
            this.ptbDealerCarta3.Visible = false;
            // 
            // ptbDealerCarta4
            // 
            this.ptbDealerCarta4.AccessibleName = "4";
            this.ptbDealerCarta4.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta4.Image")));
            this.ptbDealerCarta4.Location = new System.Drawing.Point(787, 138);
            this.ptbDealerCarta4.Name = "ptbDealerCarta4";
            this.ptbDealerCarta4.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta4.TabIndex = 61;
            this.ptbDealerCarta4.TabStop = false;
            this.ptbDealerCarta4.Visible = false;
            // 
            // ptbDealerCarta2
            // 
            this.ptbDealerCarta2.AccessibleName = "8";
            this.ptbDealerCarta2.Image = ((System.Drawing.Image)(resources.GetObject("ptbDealerCarta2.Image")));
            this.ptbDealerCarta2.Location = new System.Drawing.Point(629, 68);
            this.ptbDealerCarta2.Name = "ptbDealerCarta2";
            this.ptbDealerCarta2.Size = new System.Drawing.Size(73, 98);
            this.ptbDealerCarta2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbDealerCarta2.TabIndex = 62;
            this.ptbDealerCarta2.TabStop = false;
            this.ptbDealerCarta2.Visible = false;
            // 
            // btnComenzar
            // 
            this.btnComenzar.AccessibleName = "None";
            this.btnComenzar.BackColor = System.Drawing.Color.Transparent;
            this.btnComenzar.Image = global::veintiuno_utn.Properties.Resources.Comenzar1;
            this.btnComenzar.Location = new System.Drawing.Point(965, 407);
            this.btnComenzar.Name = "btnComenzar";
            this.btnComenzar.Size = new System.Drawing.Size(170, 155);
            this.btnComenzar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnComenzar.TabIndex = 63;
            this.btnComenzar.TabStop = false;
            this.btnComenzar.Visible = false;
            this.btnComenzar.Click += new System.EventHandler(this.btnComenzar_Click);
            // 
            // lblAccciones
            // 
            this.lblAccciones.AutoSize = true;
            this.lblAccciones.BackColor = System.Drawing.Color.Transparent;
            this.lblAccciones.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccciones.Location = new System.Drawing.Point(1495, 539);
            this.lblAccciones.Name = "lblAccciones";
            this.lblAccciones.Size = new System.Drawing.Size(308, 23);
            this.lblAccciones.TabIndex = 64;
            this.lblAccciones.Text = "Oprima el boton para apostar";
            // 
            // timer2
            // 
            this.timer2.Interval = 10;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // chartDinero
            // 
            this.chartDinero.BackColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea1";
            this.chartDinero.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartDinero.Legends.Add(legend1);
            this.chartDinero.Location = new System.Drawing.Point(30, 5);
            this.chartDinero.Name = "chartDinero";
            this.chartDinero.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            this.chartDinero.Size = new System.Drawing.Size(243, 300);
            this.chartDinero.TabIndex = 66;
            this.chartDinero.Text = "chart1";
            this.chartDinero.Visible = false;
            // 
            // FrmCartas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1924, 1055);
            this.Controls.Add(this.chartDinero);
            this.Controls.Add(this.lblAccciones);
            this.Controls.Add(this.btnComenzar);
            this.Controls.Add(this.ptbDealerCarta2);
            this.Controls.Add(this.ptbDealerCarta4);
            this.Controls.Add(this.ptbDealerCarta3);
            this.Controls.Add(this.btnApostar);
            this.Controls.Add(this.ptbDealerCarta6);
            this.Controls.Add(this.ptbDealerCarta13);
            this.Controls.Add(this.ptbDealerCarta12);
            this.Controls.Add(this.ptbDealerCarta11);
            this.Controls.Add(this.ptbDealerCarta10);
            this.Controls.Add(this.ptbDealerCarta9);
            this.Controls.Add(this.ptbDealerCarta8);
            this.Controls.Add(this.ptbDealerCarta7);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ptbDealerCarta5);
            this.Controls.Add(this.ptbDealerCarta1);
            this.Controls.Add(this.ptbCarta2Jug6);
            this.Controls.Add(this.ptbCarta1Jug6);
            this.Controls.Add(this.ptbCarta2Jug5);
            this.Controls.Add(this.ptbCarta1Jug5);
            this.Controls.Add(this.ptbCarta2Jug4);
            this.Controls.Add(this.ptbCarta1Jug4);
            this.Controls.Add(this.ptbCarta2Jug3);
            this.Controls.Add(this.ptbCarta1Jug3);
            this.Controls.Add(this.ptbCarta2Jug2);
            this.Controls.Add(this.ptbCarta1Jug2);
            this.Controls.Add(this.ptbCarta2Jug1);
            this.Controls.Add(this.ptbCarta1Jug1);
            this.Controls.Add(this.ptbDealer);
            this.Controls.Add(this.lblMess);
            this.Controls.Add(this.ptbJugador1);
            this.Controls.Add(this.ptbJugador2);
            this.Controls.Add(this.ptbJugador3);
            this.Controls.Add(this.ptbJugador5);
            this.Controls.Add(this.ptbJugador6);
            this.Controls.Add(this.ptbJugador4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ptbMesa);
            this.Controls.Add(this.panelJugador);
            this.Name = "FrmCartas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmCartas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCartas_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmCartas_FormClosed);
            this.Load += new System.EventHandler(this.frmCartas_Load);
            this.panelJugador.ResumeLayout(false);
            this.panelJugador.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDoblar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNuevaCarta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPlantarse)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnApostar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta2Jug1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCarta1Jug1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbJugador4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbMesa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbDealerCarta2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnComenzar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartDinero)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox ptbCarta1;
        private System.Windows.Forms.PictureBox ptbCarta2;
        private System.Windows.Forms.Panel panelJugador;
        private System.Windows.Forms.PictureBox ptbCarta9;
        private System.Windows.Forms.PictureBox ptbCarta8;
        private System.Windows.Forms.PictureBox ptbCarta13;
        private System.Windows.Forms.PictureBox ptbCarta12;
        private System.Windows.Forms.PictureBox ptbCarta11;
        private System.Windows.Forms.PictureBox ptbCarta10;
        private System.Windows.Forms.PictureBox ptbCarta7;
        private System.Windows.Forms.PictureBox ptbCarta6;
        private System.Windows.Forms.PictureBox ptbCarta5;
        private System.Windows.Forms.PictureBox ptbCarta4;
        private System.Windows.Forms.PictureBox ptbCarta3;
        private System.Windows.Forms.PictureBox ptbMesa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblMess;
        private System.Windows.Forms.PictureBox ptbJugador4;
        private System.Windows.Forms.PictureBox ptbJugador6;
        private System.Windows.Forms.PictureBox ptbJugador5;
        private System.Windows.Forms.PictureBox ptbJugador3;
        private System.Windows.Forms.PictureBox ptbJugador2;
        private System.Windows.Forms.PictureBox ptbJugador1;
        private System.Windows.Forms.PictureBox ptbDealer;
        private System.Windows.Forms.PictureBox ptbCarta1Jug1;
        private System.Windows.Forms.PictureBox ptbCarta2Jug1;
        private System.Windows.Forms.PictureBox ptbCarta2Jug2;
        private System.Windows.Forms.PictureBox ptbCarta1Jug2;
        private System.Windows.Forms.PictureBox ptbCarta2Jug3;
        private System.Windows.Forms.PictureBox ptbCarta1Jug3;
        private System.Windows.Forms.PictureBox ptbCarta2Jug4;
        private System.Windows.Forms.PictureBox ptbCarta1Jug4;
        private System.Windows.Forms.PictureBox ptbCarta2Jug5;
        private System.Windows.Forms.PictureBox ptbCarta1Jug5;
        private System.Windows.Forms.PictureBox ptbCarta2Jug6;
        private System.Windows.Forms.PictureBox ptbCarta1Jug6;
        private System.Windows.Forms.PictureBox ptbDealerCarta5;
        private System.Windows.Forms.PictureBox ptbDealerCarta1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox ptbProfile;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox ptbDealerCarta7;
        private System.Windows.Forms.PictureBox ptbDealerCarta8;
        private System.Windows.Forms.PictureBox ptbDealerCarta9;
        private System.Windows.Forms.PictureBox ptbDealerCarta10;
        private System.Windows.Forms.PictureBox ptbDealerCarta11;
        private System.Windows.Forms.PictureBox ptbDealerCarta12;
        private System.Windows.Forms.PictureBox ptbDealerCarta13;
        private System.Windows.Forms.PictureBox ptbDealerCarta6;
        private System.Windows.Forms.PictureBox ptbDealerCarta3;
        private System.Windows.Forms.PictureBox ptbDealerCarta4;
        private System.Windows.Forms.PictureBox ptbDealerCarta2;
        private System.Windows.Forms.PictureBox btnDoblar;
        private System.Windows.Forms.PictureBox btnNuevaCarta;
        private System.Windows.Forms.PictureBox btnPlantarse;
        private System.Windows.Forms.PictureBox btnApostar;
        private System.Windows.Forms.Label lblApostado;
        private System.Windows.Forms.Label lblDinero;
        private System.Windows.Forms.PictureBox btnComenzar;
        private System.Windows.Forms.Label lblAccciones;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartDinero;
    }
}