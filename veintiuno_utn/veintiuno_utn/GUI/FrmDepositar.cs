﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using veintiuno_utn.BOL;
using veintiuno_utn.Entities;

namespace veintiuno_utn.GUI
{
    public partial class FrmDepositar : Form
    {
        public UsuarioBOL Ubol;
        public int Id;
        public FrmDepositar(int id)
        {
            InitializeComponent();
            Ubol = new UsuarioBOL();
            this.Id = id;
            Console.WriteLine(id);
        }
        

        private void Label6_Click(object sender, EventArgs e)
        {
            try
            {
                EUsuario u = new EUsuario();
                double d = double.Parse(txtMonto.Text);
                Ubol.Deposito(d,Id);
                this.Close();

            }
            catch(Exception ex)
            {
                lblError.Text = ex.Message;
            }
            

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            
        }

        private void Panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
