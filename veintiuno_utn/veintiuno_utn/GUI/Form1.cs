﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using veintiuno_utn.DAL;

namespace veintiuno_utn.GUI
{  
    public partial class Form1 : Form
    {
        private MesaDAL dal;
        public Form1()
        {
            InitializeComponent();
            dal = new MesaDAL();
        }
        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            dal.TextoCambiado(textBox1.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dal.InsertarText();
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = dal.VerificarText();
        }
    }
}
