﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using veintiuno_utn.BOL;
using veintiuno_utn.Entities;

namespace veintiuno_utn.GUI
{
    public partial class FrmApostar : Form
    {
        public UsuarioBOL Ubol;
        private PartidaBOL Pbol;
        public int id;
        public int id_mesa;
        public string buton;
        public FrmApostar(int id, int id_mesa, string boton)
        {
            InitializeComponent();
            Ubol = new UsuarioBOL();
            Pbol = new PartidaBOL();
            this.id = id;
            this.id_mesa = id_mesa;
            this.buton = boton;
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtApuesta_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
            try
            {
                if ((int)e.KeyChar == (int)Keys.Enter)
                {
                    double apuesta = double.Parse(txtApuesta.Text.Trim());
                    double aumento = 0;
                    EApuesta ap = new EApuesta()
                    {
                        Id_usuario = id,
                        Id_mesa = id_mesa
                    };
                    Pbol.EstadoApuesta(ap);
                    if (ap.apuesta > 0 && buton.Equals("None"))
                    {
                        aumento = ap.apuesta + apuesta;
                        int i = Ubol.ValidarApuesta(aumento, id);
                        switch (i)
                        {
                            case 2:
                                FrmDepositar frm = new FrmDepositar(id);
                                frm.ShowDialog();
                                break;
                            case 3:
                                Pbol.AumentarApuesta(id, id_mesa, aumento);
                                //Ubol.TraerApuesta(id, id_mesa,aumento);
                                break;
                        }
                      
                    }
                    else
                    {
                        int i=Ubol.ValidarApuesta(apuesta,id);
                        switch (i)
                        {
                            case 2:
                                FrmDepositar frm = new FrmDepositar(id);
                                frm.ShowDialog();
                                break;
                            case 3:
                                Ubol.TraerApuesta(id, id_mesa, apuesta);
                                break;
                        }
                       
                    }
                    //Ubol.ValidarApuesta(id, apuesta);
                    //Ubol.TraerApuesta(id, id_mesa, apuesta);
                    //}

                    this.Close();
                }
            }
            catch (Exception)
            {
                if (MessageBox.Show("Hubieron problemas, desea salir?", "Salir", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }

        public void Apuestas(EApuesta ap,double apuesta)
        {
            int a = ap.apuesta > 0 && buton.Equals("None") ? 1 :2;
            switch (a)
            {
                case 1:
                    double apuest = ap.apuesta + apuesta;
                    int v = Ubol.ValidarApuesta(apuest,id);
                    if (v == 2)
                    {
                        FrmDepositar frm = new FrmDepositar(id);
                        frm.ShowDialog();
                    }
                    else if (v == 3)
                    {
                        Pbol.AumentarApuesta(id, ap.Id_mesa, apuest);
                    }
                    break;

                case 2:
                    int v1 = Ubol.ValidarApuesta(apuesta, id);
                    if (v1 == 2)
                    {
                        FrmDepositar frm = new FrmDepositar(id);
                        frm.ShowDialog();
                    }
                    else if (v1 == 3)
                    {
                        Ubol.TraerApuesta(id, ap.Id_mesa, apuesta);
                    }
                    break;


                   

            }

        }
        private void SoloNumeros(KeyPressEventArgs e)
        {
            try
            {
                if (Char.IsNumber(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if (Char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if (Char.IsSeparator(e.KeyChar))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                throw new ArgumentException("Solo se permiten números");
            }
        }

        private void txtApuesta_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtApuesta.TextLength > 0)
            {
                lblMess.Visible = true;
            }
            else
            {
                lblMess.Visible = false;
            }
        }

        private void FrmApostar_Load(object sender, EventArgs e)
        {

        }
    }
}
