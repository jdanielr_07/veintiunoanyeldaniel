﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using veintiuno_utn.BOL;
using veintiuno_utn.Entities;

namespace veintiuno_utn.GUI
{
    public partial class FrmCorreo : Form
    {
        public EUsuario u;
        public CorreoBOL cbol;
        public EMesa me;
        public FrmCorreo(EUsuario u,EMesa mesa )
        {
            InitializeComponent();
            this.u = u;
            cbol = new CorreoBOL();
            this.me = mesa;
           

        }
        
        
        public void EnviarCorreo( )
        {
            try
            {
                cbol.EnviarCorreo(txtCorreo.Text, me.Codigo_invitacion, me.Pass);
            }catch(Exception ex)
            {

            }
            }
            

        private void TextBox1_TextChanged(object sender, EventArgs e)
        { 

        }

        private void FrmCorreo_Load(object sender, EventArgs e)
        {

        }

        private void BtnEnviarCorreo_Click(object sender, EventArgs e)
        {
            try
            {
                EnviarCorreo();
                txtCorreo.Clear();
            }
            catch(Exception ex)
            {
                
            }
           
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }
    }
}
