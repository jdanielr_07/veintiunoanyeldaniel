﻿namespace veintiuno_utn.GUI
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBoxFondo = new System.Windows.Forms.PictureBox();
            this.btnFacebook = new System.Windows.Forms.PictureBox();
            this.btnGoogle = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFondo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFacebook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGoogle)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(566, 162);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // pictureBoxFondo
            // 
            this.pictureBoxFondo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxFondo.Image = global::veintiuno_utn.Properties.Resources.Blackjack_editado2;
            this.pictureBoxFondo.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxFondo.Name = "pictureBoxFondo";
            this.pictureBoxFondo.Size = new System.Drawing.Size(911, 604);
            this.pictureBoxFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFondo.TabIndex = 1;
            this.pictureBoxFondo.TabStop = false;
            // 
            // btnFacebook
            // 
            this.btnFacebook.Image = ((System.Drawing.Image)(resources.GetObject("btnFacebook.Image")));
            this.btnFacebook.Location = new System.Drawing.Point(288, 238);
            this.btnFacebook.Name = "btnFacebook";
            this.btnFacebook.Size = new System.Drawing.Size(317, 58);
            this.btnFacebook.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnFacebook.TabIndex = 2;
            this.btnFacebook.TabStop = false;
            this.btnFacebook.Click += new System.EventHandler(this.BtnFacebook_Click);
            // 
            // btnGoogle
            // 
            this.btnGoogle.Image = ((System.Drawing.Image)(resources.GetObject("btnGoogle.Image")));
            this.btnGoogle.Location = new System.Drawing.Point(283, 315);
            this.btnGoogle.Name = "btnGoogle";
            this.btnGoogle.Size = new System.Drawing.Size(322, 64);
            this.btnGoogle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnGoogle.TabIndex = 3;
            this.btnGoogle.TabStop = false;
            this.btnGoogle.Click += new System.EventHandler(this.BtnGoogle_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(911, 604);
            this.Controls.Add(this.btnGoogle);
            this.Controls.Add(this.btnFacebook);
            this.Controls.Add(this.pictureBoxFondo);
            this.Controls.Add(this.button1);
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmPrincipal";
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFondo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFacebook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGoogle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBoxFondo;
        private System.Windows.Forms.PictureBox btnFacebook;
        private System.Windows.Forms.PictureBox btnGoogle;
    }
}