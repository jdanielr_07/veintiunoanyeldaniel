﻿using Facebook;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using veintiuno_utn.BOL;
using veintiuno_utn.Entities;

namespace veintiuno_utn.GUI
{
    public partial class FrmPrincipal : Form
    {
        public UsuarioBOL Ubol { get; set; }
        public LoginBOL Lbol { get; set; }

        public FrmPrincipal()
        {
            Ubol = new UsuarioBOL();
            Lbol = new LoginBOL();
            InitializeComponent();

        }
        private void FrmPrincipal_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Obtiene el token del usuario
        /// </summary>
        private async void LoginGoogle()
        {
            string state = Lbol.randomDataBase64url(32);

            string code_verifier = Lbol.randomDataBase64url(32);
            string code_challenge = Lbol.base64urlencodeNoPadding(Lbol.sha256(code_verifier));
            const string code_challenge_method = "S256";


            // Creates a redirect URI using an available port on the loopback address.
            string redirectURI = string.Format("http://{0}:{1}/", IPAddress.Loopback, Lbol.GetRandomUnusedPort());

            // Creates an HttpListener to listen for requests on that redirect URI.
            var http = new HttpListener();
            http.Prefixes.Add(redirectURI);
            http.Start();
        
            // Creates the OAuth 2.0 authorization request.
                        string authorizationRequest = string.Format("{0}?response_type=code&redirect_uri={1}&client_id={2}&state={3}&code_challenge={4}&code_challenge_method={5}&scope={6}",
                Lbol.authorizationEndpoint,
                System.Uri.EscapeDataString(redirectURI),
                Lbol.clientID,
                state,
                code_challenge,
                code_challenge_method,
                Lbol.scope);

            // Opens request in the browser.
            System.Diagnostics.Process.Start(authorizationRequest);

            // Waits for the OAuth authorization response.
            var context = await http.GetContextAsync();

            // Brings this app back to the foreground.
            this.Activate();

            // Sends an HTTP response to the browser.
            var response = context.Response;
            string responseString = string.Format("<html><head><meta http-equiv='refresh' content='10;url=https://google.com'></head><body>Please return to the app.</body></html>");
            var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            var responseOutput = response.OutputStream;
            Task responseTask = responseOutput.WriteAsync(buffer, 0, buffer.Length).ContinueWith((task) =>
            {
                responseOutput.Close();
                http.Stop();
                Console.WriteLine("HTTP server stopped.");
            });

            // Checks for errors.
            if (context.Request.QueryString.Get("error") != null)
            {
                return;
            }
            if (context.Request.QueryString.Get("code") == null
                || context.Request.QueryString.Get("state") == null)
            {
                return;
            }

            // extracts the code
            var code = context.Request.QueryString.Get("code");
            var incoming_state = context.Request.QueryString.Get("state");

            // Compares the receieved state to the expected value, to ensure that
            // this app made the request which resulted in authorization.
            if (incoming_state != state)
            {
                return;
            }
            // Starts the code exchange at the Token Endpoint.
            performCodeExchange(code, code_verifier, redirectURI);
            http.Close();
        }

        private void BtnFacebook_Click(object sender, EventArgs e)
        {
            FrmFacebook fb = new FrmFacebook();
           
            switch (fb.ShowDialog(this))
            {
                case DialogResult.OK:    // There was an error
                    //Get the error information

                    var f = new FacebookClient();
                    f.AccessToken = fb.access_token;
                    dynamic me = f.Get("me?fields=picture,first_name,email,id");
                    //dynamic me = f.Get("me");
                    EUsuario u = new EUsuario();
                    u.Nombre = me.first_name;
                    u.Correo = me.email;
                    u.Id_partida = 0;
                    string ug = me.id;
                    string url1 = string.Format("https://graph.facebook.com/{0}/picture", ug);
                    Image ij = Lbol.Descarga(url1);
                    u.Foto = ij;
                    u.dinero_disponible = 0;
                    Ubol.Guardar(u);                   
                    u.Id = Ubol.RetornarID(u.Correo);
                    FrmMenu menu = new FrmMenu(u);
                    menu.ShowDialog();
                    this.Hide();
                    //MessageBox.Show("There was an error or the user denied access! See log window for more information!", "Error: An error occurred", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case DialogResult.Cancel:   // User clicked cancel or closed the dialog
                    

                    break;

            }
        }
        /// <summary>
        /// Trae al informacion a un json
        /// </summary>
        /// <param name="code">codigo</param>
        /// <param name="code_verifier">codigo verificacion</param>
        /// <param name="redirectURI">url</param>
        async void performCodeExchange(string code, string code_verifier, string redirectURI)
        {
            // builds the  request
            string tokenRequestURI = "https://www.googleapis.com/oauth2/v4/token";
            string tokenRequestBody = string.Format("code={0}&redirect_uri={1}&client_id={2}&code_verifier={3}&client_secret={4}&scope=&grant_type=authorization_code",
                code,
                System.Uri.EscapeDataString(redirectURI),
                Lbol.clientID,
                code_verifier,
                Lbol.clientSecret
                );

            // sends the request
            HttpWebRequest tokenRequest = (HttpWebRequest)WebRequest.Create(tokenRequestURI);
            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8";
            byte[] _byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = _byteVersion.Length;
            Stream stream = tokenRequest.GetRequestStream();
            await stream.WriteAsync(_byteVersion, 0, _byteVersion.Length);
            stream.Close();

            try
            {
                // gets the response
                WebResponse tokenResponse = await tokenRequest.GetResponseAsync();
                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    // reads response body
                    string responseText = await reader.ReadToEndAsync();

                    // converts to dictionary
                    Dictionary<string, string> tokenEndpointDecoded = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);

                    string access_token = tokenEndpointDecoded["access_token"];
                    userinfoCall(access_token);
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // reads response body
                            string responseText = await reader.ReadToEndAsync();

                        }
                    }

                }
            }



        }


        /// <summary>
        /// Extrae la informacion del usuario de un json
        /// </summary>
        /// <param name="j">json</param>
        private void Informacion(string j)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            dynamic blogObject = js.Deserialize<dynamic>(j);
            string email = blogObject["email"];
            string nombre = blogObject["name"];
            string foto = blogObject["picture"];
            EUsuario u = new EUsuario();
            u.Nombre = nombre;
            u.Correo = email;
            u.Id_partida = 0;
            u.Foto = Lbol.Descarga(foto);
            u.dinero_disponible = 100;
            Ubol.Guardar(u);
            u.Id = Ubol.RetornarID(u.Correo);
            FrmMenu frm = new FrmMenu(u);
            this.Hide();
            frm.ShowDialog();
        }


        /// <summary>
        /// Obtiene informacion mediante el token
        /// </summary>
        /// <param name="access_token">token de acceso del usuario</param>
        async void userinfoCall(string access_token)
        {
            string userinfoRequestURI = "https://www.googleapis.com/oauth2/v3/userinfo";

            // sends the request
            HttpWebRequest userinfoRequest = (HttpWebRequest)WebRequest.Create(userinfoRequestURI);
            userinfoRequest.Method = "GET";
            userinfoRequest.Headers.Add(string.Format("Authorization: Bearer {0}", access_token));
            userinfoRequest.ContentType = "application/x-www-form-urlencoded";
            userinfoRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8";

            // gets the response
            WebResponse userinfoResponse = await userinfoRequest.GetResponseAsync();
            using (StreamReader userinfoResponseReader = new StreamReader(userinfoResponse.GetResponseStream()))
            {


                // reads response body
                string userinfoResponseText = await userinfoResponseReader.ReadToEndAsync();
                Informacion(userinfoResponseText);
            }

        }

        private void BtnGoogle_Click(object sender, EventArgs e)
        {
            try
            {
                LoginGoogle();


            }
            catch (Exception)
            {
                throw;
            }


        }
    }
}

