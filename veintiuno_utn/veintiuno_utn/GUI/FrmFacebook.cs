﻿using System;
using System.Net;
using System.Windows.Forms;
using veintiuno_utn.BOL;


namespace veintiuno_utn.GUI
{
    public partial class FrmFacebook : Form
    {


        public UsuarioBOL Ubol { get; set; }
        public Uri url;
        public string App_id = "2545681038856530";
        public String permisos = "email,user_photos,public_profile";
        private string p_access_token;
        private DialogResult result = DialogResult.OK;

        public string access_token { get { return p_access_token; } }

        public FrmFacebook()
        {
            InitializeComponent();
        }

        private void WebBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void FrmFacebook_Load(object sender, EventArgs e)
        {
            string returnURL = WebUtility.UrlEncode("https://www.facebook.com/connect/login_success.html");
            string scopes = WebUtility.UrlEncode(permisos);
            url = new Uri(string.Format("https://www.facebook.com/dialog/oauth?client_id={0}&redirect_uri={1}&response_type=token%2Cgranted_scopes&scope={2}&display=popup", new object[] { App_id, returnURL, scopes }));
            webBrowser1.Navigate(url);


        }
        /// <summary>
        /// Extrae el token
        /// </summary>
        /// <param name="inpTrimChar">string cadena</param>
        /// <param name="urlInfo">url</param>
        private void ExtractURLInfo(string inpTrimChar, string urlInfo)
        {
            string fragments = urlInfo.Trim(char.Parse(inpTrimChar)); // Trim the hash or the ? mark
            string[] parameters = fragments.Split(char.Parse("&")); // Split the url fragments / query string 

            // Extract info from url
            foreach (string parameter in parameters)
            {
                string[] name_value = parameter.Split(char.Parse("=")); // Split the input

                switch (name_value[0])
                {
                    case "access_token":
                        this.p_access_token = name_value[1];
                        break;

                }
            }
        }


        private void WebBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            // Set title
            this.Text = webBrowser1.DocumentTitle;


            // Check to see if we hit return url
            if (webBrowser1.Url.AbsolutePath == "/connect/login_success.html")
            {
                // Check for error
                if (webBrowser1.Url.Query.Contains("error"))
                {
                    // Error detected
                    this.DialogResult = System.Windows.Forms.DialogResult.Abort;
                    ExtractURLInfo("?", webBrowser1.Url.Query);
                }
                else
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    ExtractURLInfo("#", webBrowser1.Url.Fragment);


                }
                webBrowser1.Navigate(new Uri(String.Format("https://www.facebook.com/logout.php?next={0}&access_token={1}", url, access_token)));
                /////*webBrowser1.GoBack()*/
                this.Close();
                //// Close the dialog

            }
        }







        private void FrmFacebook_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = result;
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
